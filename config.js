require('dotenv').config()
module.exports = {
    cookieSecret: process.env.COOKIE_SECRET,
    secretCaptcha: process.env.SECRET_CAPTCHA,
    tokenKey: process.env.TOKEN_KEY,
    roleAdministrator: process.env.ADMINISTRATOR,
    roleCustomer: process.env.CUSTOMER,
    secretStripeToken: process.env.SECRETSTRIPETOKEN,
    successCallbackUrl: process.env.SUCCESSCALLBACKURL,
    cancelCallbackUrl: process.env.CANCELCALLBACKURL,
    billDirectory: process.env.BILLDIRECTORY,
    categoryPictureDirectory: process.env.CATEGORYPICTUREDIRECTORY,
    urlUploadedCategoryPictureDirectory: process.env.URLUPLOADEDCATEGORYPICTUREDIRECTORY,
    specialityPictureDirectory: process.env.SPECIALITYPICTUREDIRECTORY,
    urlUploadedSpecialityPictureDirectory: process.env.URLUPLOADEDSPECIALITYPICTUREDIRECTORY,
};