require('dotenv').config()
const {cookieSecret} = require('./config')
const bodyParser = require('body-parser')

//app config
const robots = require('express-robots-txt')
const express = require('express')
const app = express()

app.use(robots({
    UserAgent: '*',
    Disallow: '/admin/',
    CrawlDelay: '5',
    Sitemap: 'https://kokomie.fr/sitemap.xml',
}))

const ejs = require('ejs')
const expressLayout = require('express-ejs-layouts')
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const flash = require('express-flash')
const MongoDbStore = require('connect-mongo')
const Emitter = require('events')
const favicon = require('serve-favicon');


//database connection
const url = process.env.URL_DATABASE
mongoose.connect(url, {
    useNewUrlParser : true,
    useCreateIndex: true,
    useUnifiedTopology : true,
    useFindAndModify : true
}).catch(e => console.log(e))

const connection = mongoose.connection
connection.once('open', () => {
    console.log('Database connected.....')
}).catch(err => {console.log('Connection failed....')})

const PORT  = process.env.PORT || 3000


let mongoStore = new MongoDbStore ({
    mongoUrl : url,
    collection: 'sessions'
})

//Event emitter
const eventEmiiter = new Emitter()
app.set('eventEmitter', eventEmiiter)
const uploads = express.static(path.join(__dirname, '../uploads/'));
app.use('/uploads', uploads);

app.use(favicon(__dirname + '/public/images/favicon.ico'));

//session config
app.use(session({
    secret : cookieSecret,
    resave : false,
    store : mongoStore,
    saveUninitialized : false,
    cookie : {maxAge : 1000*60*60*24} //24 hours
}))

app.use(flash())


//Assets
app.use('/', express.static(__dirname + '/public'));
app.use('/admin/tableau-de-bord/', express.static(__dirname + '/public'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.use(express.json());

//global middleware
app.use((req, res, next) => {
    res.locals.session = req.session
    next()
})

//set template engine
app.use(expressLayout)
app.set('views', path.join(__dirname, '/resources/views'))
app.set('view engine', 'ejs')

require('./routes/web')(app);
require('./routes/api')(app);

const server = app.listen(PORT, () =>  {
    console.log(`Listening on this port ${PORT}`)
})
let  connectedSockets = 0;
let  sockets = {};
//socket
const io = require('socket.io')(server, {
    perMessageDeflate : false
})

io.on('connection', (socket) => {
    if (!sockets[socket.id]) connectedSockets++;
    sockets[socket.id]={ id: socket.id };
    console.log('connected ' + socket.id + ' count ' + connectedSockets);
    // Join
    socket.on('join', (text) => {
       socket.join(text)
    })
    socket.on('disconnect', function (data) {
        delete sockets[socket.id];
        connectedSockets--;
        console.log('disconnected ' + socket.id + ' count ' + connectedSockets );
    });

})

eventEmiiter.on('orderNewPlaced', (data) => {
    io.to('administratorRoom').emit('orderNewPlaced', data)
})

eventEmiiter.on('orderStatusChanged', (data) => {
    io.to('customer'+data.orderCustomer).emit('orderStatusChanged', data)
    io.to('administratorRoom').emit('orderStatusChanged', data)
})

