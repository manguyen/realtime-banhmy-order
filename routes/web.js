const homeController = require('../app/http/controllers/homeController')
const authController = require('../app/http/controllers/authController')
const sessionController = require('../app/http/controllers/customers/sessionController')
const cartController = require('../app/http/controllers/customers/cartController')
const customerDashboardController = require('../app/http/controllers/customers/customerDashboardController')
const orderController = require('../app/http/controllers/customers/orderController')
const adminDashboardController = require('../app/http/controllers/admin/adminDashboardController')
const specialityCategoryController = require('../app/http/controllers/admin/specialityCategoryController')
const specialityController = require('../app/http/controllers/admin/specialityController')
const openingTimeController = require('../app/http/controllers/admin/openingTimeController')
const uploadController = require('../app/http/controllers/admin/uploadController')
const adminAuth = require('../app/http/middleware/adminAuth')

function initRoutes(app) {

    //FRONT
    app.get('/', homeController().index)
    app.get('/sitemap.xml', homeController().siteMap)
    app.get('/espace-membre',authController().login)
    app.get('/accueil-membre',customerDashboardController().index)
    app.get('/votre-commande',orderController().index)
    app.post('/mustOpenSideCart',customerDashboardController().mustOpenSideCart)

    //cart
    app.get('/cart', cartController().getCart)
    app.post('/update-cart', cartController().update)
    app.post('/clear-cart', cartController().clear)

    //session
    app.post('/add-user-session', sessionController().addUserToSession)
    app.post('/edit-user-session', sessionController().editUserSession)
    app.get('/check-user-session', sessionController().isUserSessionExist)
    app.post('/edit-currentOrder-session', sessionController().editNumberCurrentOrder)

    //BACK
    //dashboard
    app.get('/admin/tableau-de-bord', adminAuth,adminDashboardController().index)
    app.get('/getNumbePlacedAndProcessingOrder', adminAuth,adminDashboardController().getPlacedAndProcessingOrder)

    //category
    app.get('/admin/categories-plats', adminAuth,specialityCategoryController().index)
    app.post('/admin/nouvelle-categorie', adminAuth,specialityCategoryController().createSpecialityCategory)
    app.post('/admin/update-category', adminAuth,specialityCategoryController().updateSpecialityCategory)
    app.post('/admin/delete-category', adminAuth,specialityCategoryController().deleteSpecialityCategory)

    //speciality
    app.get('/admin/plats', adminAuth,specialityController().index)
    app.post('/admin/nouveau-plat', adminAuth,specialityController().createSpeciality)
    app.post('/admin/update-speciality', adminAuth,specialityController().updateSpeciality)
    app.post('/admin/delete-speciality', adminAuth,specialityController().deleteSpeciality)

    //opening time
    app.get('/admin/config-horaires-ouverture', adminAuth,openingTimeController().index)
    app.post('/admin/add-new-openday', adminAuth,openingTimeController().configure)
    app.post('/admin/update-openingtime', adminAuth,openingTimeController().updateOpeningTime)

    //upload
    app.post('/admin/uploads', adminAuth,uploadController().uploadPicture)

    //logout
    app.post('/logout', authController().logout)
}

module.exports = initRoutes;