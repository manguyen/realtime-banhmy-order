
const authController = require('../app/http/controllers/authController')
const customerDashboardController = require('../app/http/controllers/customers/customerDashboardController')
const orderController = require('../app/http/controllers/customers/orderController')
const stripeController = require('../app/http/controllers/customers/stripeController')

function initRoutes(app) {
    app.post('/register', authController().postRegister)
    app.post('/login', authController().postLogin)
    app.post('/edit-account', customerDashboardController().editAccount)

    //orders
    app.post('/orders', orderController().store)
    app.post('/order', orderController().getOrder)
    app.post('/changeStatusOrder', orderController().changeOrderStatus)

    //checkout session
    app.post('/create-checkout-session', stripeController().createCheckoutSession)
    app.get('/successOrder', orderController().successOrderCallback)
}

module.exports = initRoutes;