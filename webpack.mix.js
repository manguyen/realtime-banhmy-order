let mix = require('laravel-mix');

mix.js(['resources/js/front/app.js', 'resources/js/front/animation.js'], 'public/js/app.js').sass('resources/scss/app.scss','public/css/app.css');
mix.js(
    [
        'resources/js/back/dashboard.js',
        'resources/js/back/openingTime.js',
        'resources/js/back/specialityCategory.js',
        'resources/js/back/speciality.js',
    ], 'public/js/admin.js'
    )
    .sass('resources/scss/back.scss', 'public/css/back.css');


var LiveReloadPlugin = require('webpack-livereload-plugin');

mix.webpackConfig({
    plugins: [new LiveReloadPlugin()]
});