FROM node:14-alpine

COPY package.json /data/
WORKDIR /data/
RUN npm install
ENV PATH /data/node_modules/.bin:$PATH

COPY . /data/app/
WORKDIR /data/app/

# If you are building your code for production
# RUN npm ci --only=production

EXPOSE 3000