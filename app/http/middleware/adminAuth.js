const {roleAdministrator} = require('../../../config')

function adminAuth(req,res,next) {
    if (!req.session.user || req.session.user.role !== roleAdministrator) {
        res.status(403).render('back/error/forbidden', {layout: 'back/error/errorLayout'});
    } else {
        return next()
    }
}

module.exports = adminAuth