const axios = require('axios')
var FormData = require('form-data');
const {secretCaptcha, tokenKey, roleAdministrator} = require('../../../config')
const User = require('../../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {validateEmail} = require('../../tools/regexValidate')

const moment = require('moment-timezone');
const {manageOrderTime} = require('../../../app/tools/time/manageOrderTimeService')
const OpeningTime = require('../../models/opening_time')

function authController() {
    return {
        async login(req, res) {
            const todayWeekDay = moment.tz('Europe/Paris').weekday()
            const possibleOrderTime = await manageOrderTime(todayWeekDay, moment, OpeningTime)
            if (!req.session.user) {
                res.render('front/components/auth/connexion',
                    {
                        layout: "front/layout",
                        possibleOrderTime : possibleOrderTime
                    })
            } else {
                res.redirect('/accueil-membre')
            }
        },

        async postLogin(req, res) {
            try {
                // Get user input
                const {email, password} = req.body;

                // Validate user input
                if (!(email && password)) {
                    res.status(200).send({
                        "success": false,
                        "message": "Veuillez renseigner votre email et votre mot de passe",
                    });
                }
                // Validate if user exist in our database
                const user = await User.findOne({email});

                if (user && (await bcrypt.compare(password, user.password))) {
                    // Create token
                    const tokenJwt = jwt.sign(
                        {user_id: user._id, email, role: user.role},
                        tokenKey,
                        {
                            expiresIn: "24h",
                        }
                    );

                    // save user token
                    user.token = tokenJwt;

                    const {createdAt, firstName, lastName, phone, role, token, updatedAt, _id} = user

                    if (role === roleAdministrator) {
                        var redirectURL = 'admin/tableau-de-bord/'
                    } else {
                        redirectURL = req.session.redirectURL ? req.session.redirectURL : '/'
                    }

                    res.status(200).send({
                        "success": true,
                        "message": "Ravi de te revoir " + user.firstName + " !",
                        "user": {
                            createdAt,
                            email: user.email,
                            firstName,
                            lastName,
                            role,
                            token,
                            updatedAt,
                            phone,
                            _id
                        },
                        "redirectURL": redirectURL
                    });
                } else {
                    res.status(200).send({
                        "success": false,
                        "message": "Votre e-mail ou votre mot de passe n'est pas valide",
                    });
                }
            } catch (err) {
                console.log(err);
                res.status(400).send({
                    "success": false,
                    "message": "La connexion est indisponible dû à un problème technique",
                });
            }
        },

        async postRegister(req, res) {
            const {firstName, lastName, email, password, confirmPassword, captchaToken} = req.body
            if (
                !firstName || !lastName || !email ||
                !password || !confirmPassword || !captchaToken) {
                return res.json(
                    {
                        "success": false,
                        "message": "Inscription échouée, les données n'ont pas été correctement envoyées",
                    }
                )
            }

            if (password !== confirmPassword) {
                return res.json(
                    {
                        "success": false,
                        "message": "Les mots de passes saisis ne sont pas identiques",
                    }
                )
            }
            if (!validateEmail(email)) {
                return res.json(
                    {
                        "success": false,
                        "message": "Votre e-mail n'est pas valide",
                    }
                )
            }
            var data = new FormData();
            data.append('secret', secretCaptcha);
            data.append('response', captchaToken)

            const httpsAgent = new https.Agent({
                rejectUnauthorized: false,
            })


            var config = {
                method: 'post',
                url: 'https://www.google.com/recaptcha/api/siteverify',
                headers: {
                    ...data.getHeaders()
                },
                data: data,
                httpsAgent: httpsAgent
            };


            axios(config)
                .then(async function (response) {
                    if (response.data.success === true) {
                        User.exists({email: email}, async (err, result) => {
                            if (result) {
                                return res.json(
                                    {
                                        "success": false,
                                        "message": "Inscription échouée, cet email est déjà utilisé",
                                    }
                                )
                            } else {
                                //Hash password
                                const hashedPassword = await bcrypt.hash(password, 10)
                                //Create user
                                const user = new User({
                                    firstName,
                                    lastName,
                                    email,
                                    password: hashedPassword

                                })

                                user.save().then(function () {
                                    return res.json(
                                        {
                                            "success": true,
                                            "message": "Merci " + firstName + ", votre compte a été créé avec succès",
                                        }
                                    )
                                })
                            }
                        })

                    } else {
                        return res.json(
                            {
                                "success": false,
                                "message": "Inscription échouée, la vérification du captcha n'est pas aboutie",
                            }
                        )
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        logout(req, res) {
            req.session.mustOpenSideCart = false;
            req.session.openSideCart = false;
            req.session.redirectURL = '/';

            if (req.session.user) {
                delete req.session.user
                res.status(200).send({
                    "success": true,
                    "message": "La session a été correctement fermée",
                });
            }
        }
    }
}

module.exports = authController