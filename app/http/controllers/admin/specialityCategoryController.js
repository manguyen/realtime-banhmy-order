
const SpecialityCategory = require('../../../models/speciality_category')
const fs = require('fs');
const config = require('../../../../config')


function specialityCategoryController() {
    return {

        async index(req,res) {

            try {
                const specialityCat = await SpecialityCategory.find()
                res.render('back/components/specialityCategory', {layout: "back/layout",specialityCat: specialityCat})
            } catch (e) {
                console.log(e)
            }
        },

        async createSpecialityCategory(req,res) {
            try {
                const categoryName = req.body.categoryName
                const subTitle = req.body.subTitleCategory
                const description = req.body.descriptionCategory
                const isExtra = req.body.isExtra
                const anchor = ((categoryName.toLowerCase())).replace(/\s/g, "")

                const specialityCategory = new SpecialityCategory({
                    name: categoryName,
                    subTitle:subTitle,
                    description: description,
                    isExtra : isExtra,
                    anchor: anchor,
                })
                const newSpecialityCategory = await specialityCategory.save()

                res.status(200).send({
                    "success": true,
                    "idCategory" : newSpecialityCategory._id,
                    "message": "La catégorie a été créée avec succès",
                });
            } catch (e) {
                console.log(e)
                return res.status(401).send("Internal Error");
            }
        },

        async updateSpecialityCategory(req,res) {
            try {
                const specialityCategory = await SpecialityCategory.findOne({_id: req.body.idCat})
                specialityCategory.name = req.body.nameCat
                specialityCategory.subTitle = req.body.subtitleCat
                specialityCategory.description = req.body.descriptionCat
                specialityCategory.isExtra = req.body.isExtra
                const anchor = ((req.body.nameCat.toLowerCase())).replace(/\s/g, "")
                specialityCategory.anchor = '#'+anchor
                specialityCategory.save().then(function () {
                    res.status(200).send({
                        "success": true,
                        "message": "La catégorie a été mis à jour avec succès",
                    });
                })

            } catch (e) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }


        },
        async deleteSpecialityCategory(req,res) {
            try {
                const directory = config.categoryPictureDirectory
                const entity = await SpecialityCategory.findOne({_id: req.body.idCategory})
                if (entity.picturePath) {
                    const fileName = entity.picturePath.split('/')[3]
                    fs.unlinkSync(directory+fileName)
                }

                entity.deleteOne(function () {
                    res.status(200).send({
                        "success": true,
                        "message": "La catégorie a été supprimée avec succès",
                    });
                })

            } catch (err) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }


        }
    }
}

module.exports = specialityCategoryController