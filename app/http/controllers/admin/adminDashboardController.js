const Order = require('../../../models/order')
const moment = require('moment-timezone');
const appConst = require('../../../constantes/app-constantes')


function adminDashboardController() {
    return {
        async index(req, res) {
            try {
                // start today
                const start = moment.tz('Europe/Paris').startOf('day')
                const end = moment.tz('Europe/Paris').endOf('day')

                //order placed
                const numberOrderPlaced = await Order.countDocuments({
                    status: 'order_placed',
                    createdAt: {'$gte': start, '$lte': end}
                })
                //order processing
                const numberOrderProcessing = await Order.countDocuments({
                    status: 'order_processing',
                    createdAt: {'$gte': start, '$lte': end}
                })

                //order of the day
                const numberOrderOfTheDay = await Order.countDocuments
                (
                    {
                        createdAt: {'$gte': start, '$lte': end}
                    }
                )

                //income of the day
                const result = await Order.aggregate([
                    {$match: {createdAt: {'$gte': start.toDate(), '$lte': end.toDate()}}},
                    {$group: {_id: null, total: {$sum: "$amount"}}}
                ])

                const income = result.map(function (res) {
                    return res.total
                })

                const orders = await Order.find({
                    createdAt: {'$gte': start, '$lte': end}
                }).populate('customerId', '-password').exec()

                res.render('back/components/dashboard', {
                    layout: 'back/layout',
                    data: {
                        numberOrderPlaced: numberOrderPlaced,
                        numberOrderProcessing: numberOrderProcessing,
                        numberOrderOfTheDay: numberOrderOfTheDay,
                        income: income,
                        orders: orders,
                        moment: moment,
                        appConst: appConst
                    },
                })
            } catch (e) {
                console.log(e)
            }

        },

        async getPlacedAndProcessingOrder(req, res) {
            try {
                const start = moment.tz('Europe/Paris').startOf('day')
                const end = moment.tz('Europe/Paris').endOf('day')
                //order placed
                const numberOrderPlaced = await Order.countDocuments({
                    status: 'order_placed',
                    createdAt: {'$gte': start, '$lte': end}
                })

                //order processing
                const numberOrderProcessing = await Order.countDocuments({
                    status: 'order_processing',
                    createdAt: {'$gte': start, '$lte': end}
                })


                res.status(200).send({
                    "success": true,
                    "nbOrderPlaced": numberOrderPlaced,
                    "nbOrderProcessing": numberOrderProcessing
                });
            } catch (e) {
                console.log(e)
            }

        },
    }
}

module.exports = adminDashboardController