
const OpeningTime = require('../../../models/opening_time')
const appConst = require('../../../constantes/app-constantes')

function openingTimeController() {
    return {

        async index(req, res) {

            try {
                const openingTime = await OpeningTime.find()
                res.render('back/components/openingTime', {
                    layout: "back/layout",
                    openingTime : openingTime,
                    appConst : appConst
                })
            } catch (e) {
                console.log(e)
            }
        },

        async configure(req, res) {
            try {
                const weekDay = req.body.weekDay
                const services = req.body.services
                const isClose = req.body.isClose

                const openingTime = new OpeningTime({
                    weekday: weekDay,
                    services: services,
                    isClose: isClose,
                })

                await openingTime.save()

                res.status(200).send({
                    "success": true,
                    "message": "Enregistrement avec succèss",
                });
            } catch (e) {
                console.log(e)
                return res.status(401).send("Internal Error");
            }
        },

        async updateOpeningTime(req, res) {
            try {
                console.log(req.body)
                const opt = await OpeningTime.findOne({weekday: req.body.weekday})
                opt.isClose = req.body.isClose
                opt.services = {}
                let service1 = {
                    "startTime":  req.body.startTimeFs,
                    "endTime": req.body.endTimeFs
                }

                opt.services["Service 1"] = service1

                if (req.body.startTimeSs !== '' && req.body.endTimeSs !== '') {
                    let service2 = {
                        "startTime":  req.body.startTimeSs,
                        "endTime": req.body.endTimeSs
                    }
                    opt.services["Service 2"] = service2
                }
                await opt.save()

                res.status(200).send({
                    "success": true,
                    "message": "Enregistrement avec succèss",
                });

            } catch (e) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }
        },
    }
}

module.exports = openingTimeController