
const SpecialityCategory = require('../../../models/speciality_category')
const Speciality = require('../../../models/speciality')
const fs = require('fs');
const config = require('../../../../config')

function uploadController() {
    return {

        async uploadPicture(req,res) {

            try {
                const fileData =  req.body.fileData
                const fileName =  req.body.fileName
                const pictureOf = req.body.pictureOf
                if (req.body.idCategory) {
                     var idCategory = req.body.idCategory
                }
                if (req.body.idSpeciality) {
                    var idSpeciality = req.body.idSpeciality
                }
                let buff = Buffer.from(fileData, 'base64');
                let directory=''
                let entity = null
                switch (pictureOf) {
                    case 'category':
                        directory = config.categoryPictureDirectory
                        entity = await SpecialityCategory.findOne({_id: idCategory})
                        if (entity.picturePath) {
                            const fileName = entity.picturePath.split('/')[3]
                            if (fs.existsSync(fileName)) {
                                fs.unlinkSync(directory+fileName)
                            }
                        }
                        entity.picturePath = config.urlUploadedCategoryPictureDirectory+fileName
                        break;
                    case 'speciality':
                        directory = config.specialityPictureDirectory
                        entity = await Speciality.findOne({_id: idSpeciality})
                        if (entity.picturePath) {
                            const fileName = entity.picturePath.split('/')[3]
                            if (fs.existsSync(fileName)) {
                                fs.unlinkSync(directory+fileName)
                            }
                        }
                        entity.picturePath = config.urlUploadedSpecialityPictureDirectory+fileName
                        break;
                }

                if (!fs.existsSync(directory)){
                    fs.mkdirSync(directory, {recursive: true});
                }

                fs.writeFileSync(directory+fileName, buff);
                await entity.save()

                res.status(200).send({
                    "success": true,
                    "message": "Upload picture success",
                });


            } catch (e) {
                console.log(e)
                return res.status(401).send("Internal Error");
            }
        },
    }
}

module.exports = uploadController