const Speciality = require('../../../models/speciality')
const SpecialityCategory = require('../../../models/speciality_category')
const fs = require('fs');
const config = require('../../../../config')


function specialityController() {
    return {

        async index(req, res) {

            try {
                const speciality = await Speciality.find().populate('category').exec()
                const specialityCat = await SpecialityCategory.find()
                res.render('back/components/speciality', {
                    layout: "back/layout",
                    speciality: speciality,
                    specialityCat: specialityCat
                })
            } catch (e) {
                console.log(e)
            }
        },

        async createSpeciality(req, res) {
            try {
                const specName = req.body.specName
                const specDescription = req.body.specDescription
                const specPrice = req.body.specPrice
                const specTVA = req.body.specTVA
                const idCategory = req.body.idCategory
                const specialityCategory = await SpecialityCategory.findOne({_id: idCategory})

                const speciality = new Speciality({
                    name: specName,
                    description: specDescription,
                    price: specPrice,
                    tva: specTVA,
                    category: specialityCategory._id
                })
                const newSpeciality = await speciality.save()


                specialityCategory.specialities.push(newSpeciality._id)

                await specialityCategory.save()

                res.status(200).send({
                    "success": true,
                    "idSpeciality": newSpeciality._id,
                    "message": "Le plat a été créée avec succès",
                });
            } catch (e) {
                console.log(e)
                return res.status(401).send("Internal Error");
            }
        },

        async updateSpeciality(req, res) {
            try {
                const speciality = await Speciality.findOne({_id: req.body.idSpec})
                speciality.name = req.body.specName
                speciality.price = req.body.specPrice
                speciality.description = req.body.specDescription
                speciality.tva = req.body.specTVA
                speciality.available = req.body.isAvailable

                speciality.save().then(function () {
                    res.status(200).send({
                        "success": true,
                        "message": "La catégorie a été mis à jour avec succès",
                    });
                })

            } catch (e) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }
        },
        async deleteSpeciality(req, res) {
            try {
                const directory = config.specialityPictureDirectory
                const entity = await Speciality.findOne({_id: req.body.idSpec})

                const idCat = entity.category
                const specialityCat = await SpecialityCategory.findOne({_id: idCat})
                let specialities = specialityCat.specialities
                specialities.forEach(function (item) {
                    if (item == req.body.idSpec) {
                        const index = specialities.indexOf(item);
                        if (index > -1) {
                            specialities.splice(index, 1);
                        }
                    }
                })
                specialityCat.specialities = specialities
                await specialityCat.save()

                if (entity.picturePath) {
                    const fileName = entity.picturePath.split('/')[3]
                    fs.unlinkSync(directory + fileName)
                }

                entity.deleteOne(function () {
                    res.status(200).send({
                        "success": true,
                        "message": "Le plat a été supprimée avec succès",
                    });
                })

            } catch (err) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }

        }
    }
}

module.exports = specialityController