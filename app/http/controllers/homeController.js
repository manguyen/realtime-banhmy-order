const moment = require('moment-timezone');
const SpecialityCategory = require('../../models/speciality_category')
const OpeningTime = require('../../models/opening_time')
const {manageOrderTime} = require('../../../app/tools/time/manageOrderTimeService')
function homeController() {
    return {
        async index(req,res) {
            const todayWeekDay = moment.tz('Europe/Paris').weekday()
            const possibleOrderTime = await manageOrderTime(todayWeekDay, moment, OpeningTime)
            req.session.openSideCart = req.session.mustOpenSideCart
            try {
                const specialityCategory = await SpecialityCategory.find().populate('specialities')
                res.render('front/home', {
                    layout: "front/layout",
                    specialityCategory : specialityCategory,
                    possibleOrderTime : possibleOrderTime,
                })
            } catch (e) {
                console.log(e)
            }

        },
        siteMap(req,res) {
            res.sendFile(__dirname + '/public/sitemap.xml');
        }
    }
}

module.exports = homeController