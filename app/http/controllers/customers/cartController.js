function cartController() {
    return {
        index(req, res) {
            res.render('components/customers/cart')
        },
        update(req, res) {
            let resetValue = false
            if (!req.body.operator){
                let addCartItem = false;
                if (!req.session.cart) {
                    req.session.cart = {
                        items: {},
                        extras : {},
                        totalQty: 0,
                        totalPrice: 0
                    }
                }
                let cart = req.session.cart

                if (!cart.items[req.body._id]) {
                    cart.items[req.body._id] = {
                        item: req.body,
                        qty: 1
                    }
                    cart.totalQty += 1;
                    cart.totalPrice += req.body.price
                    addCartItem = true;
                } else {
                    cart.items[req.body._id].qty += 1;
                    cart.totalQty += 1
                    cart.totalPrice += req.body.price
                }

                return res.json(
                    {
                        totalQty: req.session.cart.totalQty,
                        addCartItem : addCartItem,
                        cartTotalPrice : Number.parseFloat(cart.totalPrice).toFixed(2),
                        item : cart.items[req.body._id]
                    }
                )
            } else {
                let cart = req.session.cart
                if (!req.body.isExtra) {
                    if (req.body.operator === "minus") {
                        cart.items[req.body._id].qty > 1 ? cart.totalQty -= 1 : cart.totalQty -= 0
                        cart.items[req.body._id].qty > 1 ? cart.totalPrice -= cart.items[req.body._id].item.price :  cart.totalPrice -= 0
                        cart.items[req.body._id].qty > 1 ? cart.items[req.body._id].qty -= 1 : cart.items[req.body._id].qty -= 0
                    }

                    if (req.body.operator === "plus") {
                        cart.totalQty += 1
                        cart.totalPrice += cart.items[req.body._id].item.price
                        cart.items[req.body._id].qty += 1
                    }

                    if (req.body.operator === "delete") {
                        if (cart.items[req.body._id]) {
                            cart.totalPrice = cart.totalPrice - (cart.items[req.body._id].qty * cart.items[req.body._id].item.price)
                        }
                        cart.items[req.body._id] ? cart.totalQty -= cart.items[req.body._id].qty : cart.totalQty -= 0
                        delete cart.items[req.body._id]
                        if(Object.keys(cart.items).length ===  0) {
                            cart.extras = {}
                            cart.totalQty = 0
                            cart.totalPrice = 0
                            resetValue = true
                        }
                    }
                }  else {
                    //manage extra
                    if (req.body.operator === "minus") {
                        if (cart.extras[req.body._id]) {
                            cart.extras[req.body._id].qty > 0 ? cart.totalQty -= 1 : cart.totalQty -= 0
                            cart.extras[req.body._id].qty > 0 ? cart.totalPrice -= cart.extras[req.body._id].item.speciality.price :  cart.totalPrice -= 0
                            cart.extras[req.body._id].qty > 0 ? cart.extras[req.body._id].qty -= 1 : cart.extras[req.body._id].qty -= 0
                            if (cart.extras[req.body._id].qty === 0) {
                                delete cart.extras[req.body._id]
                            }
                        }
                    }

                    if (req.body.operator === "plus") {
                        if (cart.extras[req.body._id]) {
                            cart.totalQty += 1
                            cart.totalPrice += req.body.speciality.price
                            cart.extras[req.body._id].qty += 1
                        } else {
                            cart.extras[req.body._id] = {
                                item: req.body,
                                qty: 1
                            }
                            cart.totalQty += 1
                            cart.totalPrice += req.body.speciality.price
                        }
                    }
                }
                return res.json(
                    {
                        _id : req.body._id,
                        totalQty: req.session.cart.totalQty,
                        cartTotalPrice :  Number.parseFloat(cart.totalPrice).toFixed(2),
                        itemQty :  cart.items[req.body._id] ? cart.items[req.body._id].qty : 0,
                        itemQtyExtra :  cart.extras[req.body._id] ? cart.extras[req.body._id].qty : 0,
                        items : cart.items,
                        resetValue: resetValue
                    }
                )
            }
        },
        getCart(req, res) {
            res.status(200).send({
                "success": true,
                "cart": req.session.cart,
                "user" : req.session.user
            });
        },
        clear(req, res) {
            if (req.session.cart) {
                delete req.session.cart
            }
            res.status(200).send({
                "success": true,
                "text" : "Cart clear with success"
            });
        }
    }
}

module.exports = cartController