const Order = require('../../../models/order')
const jwt = require("jsonwebtoken");
const {tokenKey, roleAdministrator} = require('../../../../config')
const moment = require('moment-timezone')
const {generatePdf} = require('../../../tools/bill/billGeneratorService')
const SpecialityCategory = require('../../../models/speciality_category')
const {manageOrderTime} = require('../../../tools/time/manageOrderTimeService')
const OpeningTime = require('../../../models/opening_time')

function orderController() {
    return {
        async index(req, res) {
            if (!req.session.user) {
                req.session.redirectURL = '/votre-commande'
                res.redirect('/espace-membre')
            } else {
                const orders = await Order.find({
                    customerId: req.session.user._id,
                    status: {$ne: 'order_delivered'}
                }).sort({createdAt: -1})
                const specialityCategory = await SpecialityCategory.find().populate('specialities')
                const todayWeekDay = moment.tz('Europe/Paris').weekday()
                const possibleOrderTime = await manageOrderTime(todayWeekDay, moment, OpeningTime)
                res.render('front/components/customers/order', {
                    layout: "front/layout",
                    orders: orders,
                    moment: moment,
                    specialityCategory: specialityCategory,
                    possibleOrderTime: possibleOrderTime,
                })

            }
        },
        successOrderCallback(req, res) {

            const deliveredTs =  req.session.orderTime
            const date = new Date(deliveredTs * 1000).toISOString()
            const deliveredTime = moment.tz(date, 'Europe/Paris')

            try {
                Order.find().exec(function (err, results) {
                    let orderNumber = results.length

                    const order = new Order({
                        customerId: req.session.user._id,
                        orderNumber: ++orderNumber,
                        items: req.session.cart.items,
                        extras: req.session.cart.extras,
                        comment: req.session.comment,
                        amount: Number.parseFloat(req.session.cart.totalPrice).toFixed(2),
                        paymentStatus: 'PAID',
                        paymentMethod: 'online',
                        deliveredTime: deliveredTime
                    })
                    order.save().then(function () {
                        !req.session.currentOrder ? req.session.currentOrder = 1 : req.session.currentOrder += 1

                        //Emit event
                        const eventEmitter = req.app.get('eventEmitter')
                        eventEmitter.emit('orderNewPlaced', {
                            name: req.session.user.firstName + " " + req.session.user.lastName,
                            order: order
                        })

                        const cb = order.amount
                        generatePdf(order, req.session.user, moment, 'Internet',cb)

                        delete req.session.cart
                        delete req.session.mustOpenSideCart
                        delete req.session.openSideCart
                        delete req.session.orderTime
                        delete req.session.comment

                        res.redirect('/votre-commande')
                    })
                })

            } catch (err) {
                console.log(err)
            }

        },

        async store(req, res) {
            const token = req.body.token

            if (!token) {
                return res.status(403).send("A token is required for authentication");
            }

            const deliveredTs = req.body.orderTimeValue
            const date = new Date(deliveredTs * 1000).toISOString()
            const deliveredTime = moment.tz(date, 'Europe/Paris')

            try {
                const decoded = jwt.verify(token, tokenKey);
                if (decoded.user_id === req.body.user._id) {
                    Order.find().exec(function (err, results) {
                        let orderNumber = results.length

                        const order = new Order({
                            customerId: req.body.user._id,
                            orderNumber : ++orderNumber,
                            items: req.body.cart.items,
                            extras: req.body.cart.extras,
                            comment: req.body.comment,
                            amount: Number.parseFloat(req.body.cart.totalPrice).toFixed(2),
                            paymentStatus: 'NOPAID',
                            paymentMethod: 'COD',
                            deliveredTime: deliveredTime
                        })
                        order.save().then(function () {
                            !req.session.currentOrder ? req.session.currentOrder = 1 : req.session.currentOrder += 1

                            //Emit event
                            const eventEmitter = req.app.get('eventEmitter')
                            eventEmitter.emit('orderNewPlaced', {
                                name: req.body.user.firstName + " " + req.body.user.lastName,
                                order: order
                            })

                            generatePdf(order, req.body.user, moment, 'Internet')

                            delete req.session.mustOpenSideCart
                            delete req.session.openSideCart

                            res.status(200).send({
                                "success": true,
                                "message": "Order placed with success",
                                "orderId": order._id
                            });
                        })
                    })
                } else {
                    return res.status(401).send("Invalid Token");
                }
            } catch (err) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }
        },

        async getOrder(req, res) {
            const token = req.body.token

            if (!token) {
                return res.status(403).send("A token is required for authentication");
            }

            try {
                const decoded = jwt.verify(token, tokenKey);
                if (decoded.user_id === req.body.userId || decoded.role === roleAdministrator) {
                    const order = await Order.findOne({
                        _id: req.body.orderId
                    })
                    res.status(200).send({
                        "success": true,
                        "order": order,
                    });
                } else {
                    return res.status(401).send("Invalid Token");
                }
            } catch (err) {
                console.log(err)
                return res.status(401).send("Internal Error");
            }
        },

        async changeOrderStatus(req, res) {
            const token = req.body.token
            const idOrder = req.body.orderId
            const orderStatus = req.body.orderStatus
            if (!token) {
                return res.status(403).send("A token is required for authentication");
            }

            try {
                const decoded = jwt.verify(token, tokenKey);
                if (decoded.role === roleAdministrator) {
                    const order = await Order.findOne({_id: idOrder})
                    order.status = orderStatus
                    if (orderStatus === 'order_delivered') {
                        order.paymentStatus = 'PAID'
                    }
                    order.save().then(function () {
                        //Emit event
                        const eventEmitter = req.app.get('eventEmitter')
                        eventEmitter.emit('orderStatusChanged', {
                            orderId: order._id,
                            orderStatus: order.status,
                            orderCustomer: order.customerId
                        })

                        res.status(200).send({
                            "success": true,
                            "message": "Le statut de la commande a été mis à jour",
                        });
                    })


                }
            } catch (err) {
                console.log(err)
                return res.status(401).send("Invalid Token");
            }
        },

    }
}

module.exports = orderController