
function sessionController() {
    return {
        addUserToSession(req, res) {
            if (!req.session.user) {
                    req.session.user = {
                        createdAt: req.body.createdAt,
                        email: req.body.email,
                        phone: req.body.phone,
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        role: req.body.role,
                        token: req.body.token,
                        updateAt: req.body.updateAt,
                        _id: req.body._id,
                    }
            }

            res.status(200).send({
                "success": true,
                "message": "La session a été correctement créée",
            });
        },

        editUserSession(req, res) {

            req.session.user.firstName = req.body.fname
            req.session.user.lastName = req.body.lname
            req.session.user.email = req.body.email
            req.session.user.phone = req.body.phone

            res.status(200).send({
                "success": true,
                "message": "Vos données ont été correctement mises à jour",
            });
        },

        isUserSessionExist(req, res) {

            const isExist = !!req.session.user

            res.status(200).send({
                "success": true,
                "isExist": isExist,
                "user": req.session.user,
            });
        },

        editNumberCurrentOrder(req, res) {

            req.session.currentOrder === 1 ? delete req.session.currentOrder : req.session.currentOrder -=1

            res.status(200).send({
                "success": true,
                "message": "Vos données ont été correctement mises à jour",
            });
        },
    }
}

module.exports = sessionController