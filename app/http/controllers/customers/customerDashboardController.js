const jwt = require("jsonwebtoken");
const moment = require('moment-timezone');
const {tokenKey} = require('../../../../config')
const User = require('../../../models/user')
const SpecialityCategory = require('../../../models/speciality_category')
const {manageOrderTime} = require('../../../tools/time/manageOrderTimeService')
const OpeningTime = require('../../../models/opening_time')

function customerDashboardController() {
    return {
        async index(req, res) {
            req.session.mustOpenSideCart = false;
            req.session.openSideCart = false;
            const todayWeekDay = moment.tz('Europe/Paris').weekday()
            const specialityCategory = await SpecialityCategory.find().populate('specialities')
            const possibleOrderTime = await manageOrderTime(todayWeekDay, moment, OpeningTime)

            if (req.session.user) {
                res.render('front/components/customers/account-detail', {
                    layout : "front/layout",
                    specialityCategory: specialityCategory,
                    possibleOrderTime: possibleOrderTime,
                })
            } else {
                res.redirect('/espace-membre')
            }
        },

        async editAccount(req,res) {
            const token = req.body.token
            const userId = req.body.idUser
            const fname = req.body.fname
            const lname = req.body.lname
            const email = req.body.email
            const telephone = req.body.phone
            if (!token) {
                return res.status(403).send("A token is required for authentication");
            }

            try {
                const decoded = jwt.verify(token, tokenKey);
                if (decoded.user_id === userId ) {
                    const user = await User.findOne({_id: userId})
                    user.firstName = fname
                    user.lastName = lname
                    user.email = email
                    user.phone = telephone

                    user.save().then(function () {
                        res.status(200).send({
                            "success": true,
                            "message": "Vos données ont été mises à jour",
                        });
                    })

                }
            } catch (err) {
                console.log(err)
                return res.status(401).send("Invalid Token");
            }
        },

        mustOpenSideCart (req, res) {
            req.session.mustOpenSideCart = true
            res.status(200).send({
                "success": true,
                "isOpen": req.session.mustOpenSideCart,
            });
        },
    }
}
module.exports = customerDashboardController