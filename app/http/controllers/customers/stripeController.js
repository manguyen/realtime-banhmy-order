const config = require('../../../../config')


const stripe = require('stripe')(config.secretStripeToken)

function stripeController() {
    return {
        async createCheckoutSession(req, res) {
            try {
                if (!req.session.user) {
                    res.status(200).send({
                        "success": false,
                        "urlRedirect": "/espace-membre",
                    });
                } else {
                    const session = await stripe.checkout.sessions.create({
                        payment_method_types: ['card'],
                        line_items: [{
                            price_data: {
                                currency: 'eur',
                                product_data: {
                                    name: 'Le montant de votre commande',
                                },
                                unit_amount: Number.parseFloat(req.session.cart.totalPrice).toFixed(2) * 100,
                            },
                            quantity: 1,
                        }],
                        mode: 'payment',
                        success_url: config.successCallbackUrl,
                        cancel_url: config.cancelCallbackUrl,
                    });

                    req.session.orderTime = req.body.orderTimeValue
                    req.session.comment = req.body.comment

                    res.status(200).send({
                        "success": true,
                        "urlRedirect": session.url,
                    });
                }

            } catch (e) {
                console.log(e)
                res.status(400).send({
                    "success": false,
                });
            }
        }
    }
}

module.exports = stripeController