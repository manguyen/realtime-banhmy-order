const convertStatusOrder  =  {
    "status" :  {
        "order_placed" : "En attente",
        "order_processing" : "En cours",
        "order_done" : "Terminé",
        "order_delivered" : "Livré",
    },
    "image" :  {
        "order_placed" : "img/placed.gif",
        "order_processing" : "img/processing.gif",
        "order_done" : "img/finished.gif",
        "order_delivered": "img/finished.gif"
    },
    "paymentStatus" :  {
        "PAID" : "Payé",
        "NOPAID" : "Non Payé",
    },
}

const weekdayNumber  =  {
   0 : "Dimanche",
   1 : "Lundi",
   2 : "Mardi",
   3 : "Mercredi",
   4 : "Jeudi",
   5 : "Vendredi",
   6 : "Samedi",
}

module.exports =
    {
        convertStatusOrder : convertStatusOrder,
        weekdayNumber : weekdayNumber
    }

;