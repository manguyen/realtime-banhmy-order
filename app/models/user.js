const mongoose = require('mongoose')
const Schema = mongoose.Schema


const userSchema = new Schema({
    firstName : {type : String, require : true},
    lastName : {type : String, require : true},
    email : {type : String, require : true, unique: true},
    phone : {type : String, require : false},
    password : {type : String, require : true},
    role : {type : String, default: "customer"},
    token : {type : String},
}, {timestamps : true})

const User = mongoose.model('User', userSchema)

module.exports = User