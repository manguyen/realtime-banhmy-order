const mongoose = require('mongoose')
const Schema = mongoose.Schema


const openingTimeSchema = new Schema({
    weekday: {
        type : Number, required : true
    },
    services : {
        type : Object, required: true
    },
    isClose : {type : Boolean, default : false},
}, {timestamps: true})

const OpeningTime = mongoose.model('OpeningTime', openingTimeSchema)


module.exports = OpeningTime