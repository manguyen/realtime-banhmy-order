const mongoose = require('mongoose')
const Schema = mongoose.Schema


const orderSchema = new Schema({
    customerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    orderNumber: {
        type : Number, required : true
    },
    items : {
        type : Object, required: true
    },
    extras : {
        type : Object, default: {}
    },
    amount: {
        type : Number, required : true
    },
    paymentType : { type : String, default : 'CB'},
    type :  { type : String, default : 'Internet'},
    comment :  { type : String, required : false},
    status : {type: String, default: 'order_placed'},
    paymentStatus : {type: String, required : true},
    paymentMethod : {type: String, required : true},
    deliveredTime: {type: Date , required: true}
}, {timestamps: true})

const Order = mongoose.model('Order', orderSchema)

module.exports = Order