const mongoose = require('mongoose')
const Schema = mongoose.Schema


const specialityCategorySchema = new Schema({
    name : {type : String, require : true},
    specialities: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Speciality',
        default: []
    }],
    isExtra : {type : Boolean, default : false},
    subTitle :{type : String, require : true},
    description: {type : String, require : true},
    anchor : {type : String, require : true},
    picturePath: {type: String, required: false}

},{timestamps: true})

const SpecialityCategory = mongoose.model('SpecialityCategory', specialityCategorySchema, 'speciality_category')

module.exports = SpecialityCategory