const mongoose = require('mongoose')
const Schema = mongoose.Schema


const specialitySchema = new Schema({
    name : {type : String, require : true},
    description : {type : String, require : true},
    price : {type : Number, require : true},
    tva : {type : Number, require : true},
    available : {type : Boolean, default : true},
    picturePath : {type : String, require : false},
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SpecialityCategory',
        required : true
    },
})

const Speciality = mongoose.model('Speciality', specialitySchema, 'speciality')

module.exports = Speciality