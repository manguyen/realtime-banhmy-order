const fs = require('fs')
const puppeteer = require('puppeteer')
const ejs = require('ejs')
const appConst = require('../../constantes/app-constantes')
const {billDirectory} = require('../../../config')
const QRCode = require('qrcode')

async function getTemplateHtml() {
    console.log("Loading template file in memory")
    try {
        return await fs.readFileSync('./app/tools/bill/template.ejs', 'utf8');
    } catch (err) {
        return Promise.reject("Could not load html template");
    }
}
async function generatePdf(order, client, moment, from, cb, cash, tr) {
    const qrcode = await QRCode.toDataURL(JSON.stringify(order._id))
    getTemplateHtml().then(async (res) => {
        const paymentStatus =  appConst.convertStatusOrder.paymentStatus[order.paymentStatus]
        const template = ejs.render(res, {order, client, moment,from, paymentStatus, cb, cash, tr, qrcode})
        const browser = await puppeteer.launch({
            executablePath: '/usr/bin/chromium-browser',
            args: ['--no-sandbox'],
        });
        const page = await browser.newPage()
        await page.setContent(template)

        const date = moment.tz('Europe/Paris').format('DDMMY')
        var dir = billDirectory+date
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir, {recursive: true});
        }

        await page.pdf({ path: dir+'/'+order._id+'-bill.pdf', width : '80mm', })
        await browser.close();
        console.log("Bill Generated")
    }).catch(err => {
        console.error(err)
    });
}

module.exports = {
    generatePdf
}