

async function manageOrderTime(weekdayToday, moment, optModel) {

    try {
        let stop = true
        let weekday = weekdayToday
        let services = {}
        let turnBackNumber = 0

        while (stop) {
            if (weekday > 6) {
                turnBackNumber = weekday
                weekday = 0
            }
            const opt = await optModel.findOne({
                weekday: weekday
            }).exec()

            /**
             * Si c'est une journée fermée, on passe à la journée suivante
             */
            if (opt.isClose === true) {
                weekday+=1
            } else {
                /**
                 * Si c'est une journée ouverte, on vérifie les horaires
                 */

                Object.keys(opt.services).forEach(function (k) {
                    const timeNow = moment.tz('Europe/Paris')
                    let timeStampNow = Date.parse(timeNow) / 1000
                    let startTime = opt.services[k].startTime
                    let endTime = opt.services[k].endTime

                    let day = new Date()
                    if (turnBackNumber === 0) {
                        day.setDate(day.getDate() + (weekday - weekdayToday));
                    }  else {
                        day.setDate(day.getDate() + (turnBackNumber - weekdayToday));
                    }

                    let stringDate = day.toISOString().split('T')[0]

                    let reformatDate = stringDate.split('-')[2] + '/' + stringDate.split('-')[1] + "/" + stringDate.split('-')[0]

                    let stringStartDate = stringDate + " " + startTime
                    let stringEndDate = stringDate + " " + endTime

                    let momentStartTime = moment.tz(stringStartDate, 'Europe/Paris')

                    let momentEndTime = moment.tz(stringEndDate, 'Europe/Paris')

                    let timeStampStartTime = Date.parse(momentStartTime) / 1000
                    let timeStampEndTime = Date.parse(momentEndTime) / 1000

                    if (timeStampNow < timeStampEndTime) {
                        if (timeStampNow < timeStampStartTime) {
                        } else {
                            timeStampStartTime = timeStampNow

                        }
                        let orderTimeObject = buildOrderTime(timeStampStartTime, timeStampEndTime, moment)
                        let labelDate
                        switch (weekday - weekdayToday) {
                            case 0:
                                labelDate = "Aujourd'hui "
                                break
                            case 1 :
                                labelDate = "Demain "
                                break
                            case 2:
                                labelDate = "Après-demain "
                                break
                            default:
                                labelDate = ""
                                break
                        }
                        if (Object.keys(orderTimeObject).length > 0) {
                            if (!services.hasOwnProperty(labelDate + reformatDate)) {
                                services[labelDate + reformatDate] = {}
                            }

                            services[labelDate + reformatDate][k] = orderTimeObject
                        }
                    }
                })

                if (Object.keys(services).length === 2) {
                    stop = false
                } else {
                    weekday++
                }
            }
            if (turnBackNumber !== 0) {
                turnBackNumber++
            }
        }
        return services

    } catch (e) {
        console.log(e)
    }

    function buildOrderTime(tsStartTime, tsEndTime, moment) {
        let orderTime = {}
        let needToContinue = true

        let startTime = tsStartTime
        let endTime = tsEndTime

        if (startTime > (endTime - 15 * 60)) {
            needToContinue = false
        } else {
            startTime = tsStartTime + 15 * 60
        }

        while (needToContinue) {
            let date = new Date(startTime * 1000).toISOString()
            let hour = moment.tz(date, 'Europe/Paris').hours()
            let min = moment.tz(date, 'Europe/Paris').minutes()
            let time = ''
            if (min < 10) {
                time = hour + ':0' + min
            } else {
                time = hour + ':' + min
            }
            orderTime[startTime] = time
            if (startTime > (endTime - 15 * 60)) {
                needToContinue = false
            } else {
                startTime += 15 * 60
            }
        }
        return orderTime
    }
}

module.exports = {
    manageOrderTime
}