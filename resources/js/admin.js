import axios from "axios";
import Noty from "noty";
import moment from 'moment-timezone'

let file = null
let btnLogout = document.querySelector('#logout')
let btnCreateCategory = document.getElementById('createCategorySpeciality')
let catNameInput = document.getElementById('editCategoryName')
let catSubtitleInput = document.getElementById('editCategorySubtitle')
let catDescriptionInput = document.getElementById('editCategoryDescription')
let catPicturePathInput = document.getElementById('editCategoryPicturePath')
let idCatNameInput = document.getElementById('editIdCategoryName')
let reader = {}

async function logout() {
    try {
        await axios.post('/logout')
        window.location = "/espace-membre";

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}


async function createSpecialityCategory() {
    try {
        let categoryName = document.querySelector("input[name='categoryName']").value;
        let subTitleCategory = document.querySelector("input[name='subTitleCategory']").value;
        let descriptionCategory = document.querySelector("textarea[name='descriptionCategory']").value;

        if (file === null || categoryName==='' || subTitleCategory==='' || descriptionCategory==='') {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Tous les champs sont obligatoires"
            }).show();
        } else {
            const res = await axios.post('/admin/nouvelle-categorie', {
                "categoryName" : categoryName,
                "subTitleCategory" : subTitleCategory,
                "descriptionCategory" : descriptionCategory,
            })

            if (res.data.success === true) {
                const idCategoryCreated = res.data.idCategory
                //add picture
                await start_upload(file,idCategoryCreated, "category")
                new Noty({
                    timeout: 2000,
                    type: "success",
                    text: res.data.message
                }).show();
                //reset file
                file = null
                sleep(2000).then(() => {
                    window.location.reload()
                });
            }
        }
    } catch (e) {
        console.log(e)
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}


async function updateNumbePlacedAndProcessingOrder (data) {
    try {
        const result = await axios.get('/getNumbePlacedAndProcessingOrder')
        const nbOrderPlaced = result.data.nbOrderPlaced
        const nbOrderProcessing = result.data.nbOrderProcessing
        document.getElementById('orderPlaced').innerText = nbOrderPlaced
        document.getElementById('orderProcessing').innerText = nbOrderProcessing
        if (data.orderStatus === 'order_delivered') {
            if (document.getElementById(`${'line_'+data.orderId}`).classList.contains('nopaidLine')){
                document.getElementById(`${'line_'+data.orderId}`).classList.remove('nopaidLine')
                document.getElementById(`${'line_'+data.orderId}`).classList.add('paidLine')
                document.getElementById('paymentStatus_'+data.orderId).innerHTML = 'Payé'
            }
        }
    } catch (e) {
        console.log(e)
    }

}

async function deleteCategorySpeciality(idCat) {
    try {
        if (confirm('Vous êtes sûr de vouloir supprimer cette catégorie ?'))  {
            const res = await axios.post('/admin/delete-category', {
                "idCategory": idCat,
            })

            document.getElementById("row-"+idCat).remove()

            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();
        }

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}


async function processChangeOrderStatus(orderStatus, orderId) {
    try {
        const session = await axios.get('/check-user-session')

        const res = await axios.post('/changeStatusOrder', {
            "orderStatus": orderStatus,
            "orderId": orderId,
            "token": session.data.user.token
        })

        new Noty({
            timeout: 2000,
            type: "success",
            text: res.data.message
        }).show();
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}

async function changeStatus(orderStatus, orderId, paymentStatus) {
    try {
        if (orderStatus === 'order_delivered') {
            if (paymentStatus === 'NOPAID') {
                if (confirm('Cette commande a été payée ?'))  {
                    await processChangeOrderStatus(orderStatus, orderId);
                }
            } else {
                await processChangeOrderStatus(orderStatus, orderId);
            }
        } else {
            await processChangeOrderStatus(orderStatus, orderId);
        }
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }

}

if (btnLogout) {
    btnLogout.addEventListener('click', (e) => {
        logout()
    })
}


if (btnCreateCategory) {
    btnCreateCategory.addEventListener('click', (e) => {
        e.preventDefault();
        createSpecialityCategory()
    })
}



document.querySelectorAll('.statusChange').forEach(function (elt) {
    elt.addEventListener('change', function () {
        const paymentStatus = elt.dataset.paymentstatus
        changeStatus(elt.value, elt.id, paymentStatus)
    })
})


document.querySelectorAll('.editModalOpen').forEach(function (elt) {
    elt.addEventListener('click', function () {
        const idCat = elt.dataset.idcategory
        const nameCat = elt.dataset.namecategory
        const subTitleCat = elt.dataset.subtitlecategory
        const descriptionCat = elt.dataset.descriptioncategory
        const picturePathCat = elt.dataset.picturepathcategory
        catNameInput.value = nameCat
        catSubtitleInput.value = subTitleCat
        catDescriptionInput.innerText = descriptionCat
        catPicturePathInput.src = picturePathCat
        idCatNameInput.value = idCat

    })
})

document.querySelectorAll('.deleteSpecialityCat').forEach(function (elt) {
    elt.addEventListener('click', function () {
        const idCat = elt.dataset.idcategory
        deleteCategorySpeciality(idCat)
    })
})



document.querySelectorAll('.editCategory').forEach(function (elt) {
    elt.addEventListener('click', async function () {
        try {
            const idCat = idCatNameInput.value
            const nameCat = catNameInput.value
            const subtitleCat = catSubtitleInput.value
            const descriptionCat = catDescriptionInput.value

            const res = await axios.post('/admin/update-category', {
                "idCat": idCat,
                "nameCat": nameCat,
                "subtitleCat": subtitleCat,
                "descriptionCat": descriptionCat,
            })

            document.getElementById('catname_'+idCat).innerText = nameCat
            document.getElementById('catsubtitle_'+idCat).innerText = subtitleCat

            if (file) {
                console.log(file)
                await start_upload(file,idCat, "category")
                file = null
            }

            sleep(2000).then(() => {
                window.location.reload()
            });
            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();

            //document.getElementById('catImage_'+idCat).src = nameCat
        } catch (e) {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Une erreur s'est produite"
            }).show();
        }


    })
})

function addRow(tableID, data) {
    // Get a reference to the table
    let tableRef = document.getElementById(tableID);

    // Insert a row at the end of the table
    let newRow = tableRef.insertRow(-1);

    newRow.id = 'line_'+data.order._id

    newRow.classList.add('lineOrderTable')

    if (data.order.paymentStatus === 'PAID') {
        newRow.classList.add('paidLine')
    } else{
        newRow.classList.add('nopaidLine')
    }

    // Insert a cell in the row at index 0
    let  orderColumn =  newRow.insertCell(0);
    let  clientColumn = newRow.insertCell(1);
    let  paymentStatusColumn = newRow.insertCell(2);
    let  amountColumn = newRow.insertCell(3);
    let  createdAtColumn =    newRow.insertCell(4);
    let  deliveredAtColumn =  newRow.insertCell(5);
    let  orderStatusColumn =  newRow.insertCell(6);



    // Append a text node to the cell
    let  contentOrder = ''
    Object.keys(data.order.items).forEach(function (k) {
        let item = '<p>'+ data.order.items[k].item.name+'- Qté: ' +data.order.items[k].qty+'</p>'
        contentOrder+=item
    })
    let client = document.createTextNode(data.name);
    let paymentStatus = `<span id="paymentStatus_${data.order._id}">${data.order.paymentStatus ==='PAID' ? 'Payé' : 'Non Payé'}</span>`
    let amount = document.createTextNode(data.order.amount + ' €');
    let orderCreatedAt = document.createTextNode(moment.tz(data.order.createdAt, "Europe/Paris").format('D/M/Y HH:mm'));
    let orderDeliveredAt = document.createTextNode(moment.tz(data.order.deliveredTime, "Europe/Paris").format('D/M/Y HH:mm'));
    let orderCurrentStatus = ` <fieldset class="form-group">
                                    <select class="form-select statusChange" id="${data.order._id}" data-paymentstatus="${data.order.paymentStatus}">
                                         <option value="order_placed" selected>En attente</option>
                                         <option value="order_processing" >En cours</option>
                                         <option value="order_done">Terminé</option>
                                         <option value="order_delivered">Livré</option>
                                    </select>
                               </fieldset>`;

    orderColumn.innerHTML = contentOrder;
    clientColumn.appendChild(client);
    paymentStatusColumn.innerHTML = paymentStatus;
    amountColumn.appendChild(amount);
    createdAtColumn.appendChild(orderCreatedAt);
    deliveredAtColumn.appendChild(orderDeliveredAt);
    orderStatusColumn.innerHTML = orderCurrentStatus;

    document.getElementById(`${data.order._id}`).addEventListener('change', function () {
        const paymentStatus = this.dataset.paymentstatus
        changeStatus(this.value, this.id, paymentStatus)
    })
}


//socket
let socket = io()
// Join
socket.emit('join', 'administratorRoom')

socket.on('orderNewPlaced', (data) => {

    const orderPlaced = document.getElementById('orderPlaced')
    const incomeOfTheDay = document.getElementById('incomeOfTheDay')
    const numberOrderOfTheDay = document.getElementById('numberOrderOfTheDay')

    ++orderPlaced.textContent
    ++numberOrderOfTheDay.textContent
    let incomeAmount = parseInt(incomeOfTheDay.textContent)  + data.order.amount
    incomeOfTheDay.innerHTML = incomeAmount

     addRow('currentDashboardOrderTable', data)


    new Noty({
        timeout: 2000,
        type: "success",
        text: "Une nouvelle commande de " + data.order.amount + " € vient d'être passée par " + data.name
    }).show();
})


socket.on('orderStatusChanged', (data) => {
    updateNumbePlacedAndProcessingOrder(data)
})


// sleep time expects milliseconds
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

FilePond.registerPlugin(
    FilePondPluginImagePreview,
);


const pond = FilePond.create(document.querySelector('.image-preview-filepond'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});


const pondModal = FilePond.create(document.querySelector('.image-preview-filepond-modal'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});

pond.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    file = fileAdded.file
});

pond.on('removefile', () => {

    file = null
});

pondModal.on('removefile', () => {

    file = null
});

pondModal.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    file = fileAdded.file
});

async function start_upload(file, idCategory, pictureOf) {
    reader = new FileReader();
    await upload_file(file, idCategory, pictureOf);
}

async function upload_file(file, idCategory, pictureOf) {
    reader.onloadend = async function (event) {
        await axios.post('/admin/uploads', {
            "fileData": event.target.result.split(',')[1],
            "fileName": file.name,
            "pictureOf": pictureOf,
            "idCategory": idCategory,
        })
    }
    reader.readAsDataURL(file)
}


