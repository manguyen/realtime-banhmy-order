import axios from 'axios'
import Noty from 'noty'
import Moment from 'moment-timezone'
//socket
let socket = io()

let minusQtyItem = document.querySelectorAll('.minus-qty-item');
let minusQtyItemExtra = document.querySelectorAll('.minus-qty-item-extra');
let plusQtyItem = document.querySelectorAll('.plus-qty-item');
let plusQtyItemExtra = document.querySelectorAll('.plus-qty-item-extra');
let deleteItemCart = document.querySelectorAll('.cart-item-del-btn');
let addToCart = document.querySelectorAll('.add-cart');
let cartList = document.querySelector('.cart-list')
let cartCounter = document.querySelector('#cartCounter')
let btnSignUp = document.querySelector('#btnSignUp')
let btnSignIn = document.querySelector('#btnSignIn')
let btnLogout = document.querySelector('#logout')
let btnEditAccount = document.querySelector('#editAccountData')
let btnOrderBtn = document.querySelector('#cart-btn')
let btnOrderCODBtn = document.querySelector('#cart-btn-cod')
let selectCurrentOrders = document.querySelector('#currentOrders')
const appConst = require('../../../app/constantes/app-constantes')
let addExtra = document.querySelector('#extradAdd')
let orderDateContent = document.querySelector('#orderDateContent')
let weekDayCart = document.querySelector('#weekDayCart')
let orderTimeCart = document.querySelector('#orderTimeCart')


const tabStatusOrdered = {
    "order_placed": "en attente",
    "order_processing": "en cours de préparation",
    "order_done": "prête",
    "order_delivered": "livrée"
}

const tabImgStatus = {
    "order_placed": "img/placed.gif",
    "order_processing": "img/processing.gif",
    "order_done": "img/finished.gif",
    "order_delivered": "img/finished.gif",
}


//add cart product
async function updateCart(spec, operator, isExtra, speciality) {
    try {
        if (!operator) {
            let cartCounter = document.querySelector('#cartCounter')
            let emptyCart = document.querySelector('#empty-cart')
            if (emptyCart !== null) {
                emptyCart.remove()
            }
            let res = await axios.post('/update-cart', JSON.parse(spec))
            cartCounter.innerText = res.data.totalQty
            const elt = res.data.item
            if (res.data.addCartItem === true) {
                addExtra.style.display = 'block'
                orderDateContent.style.visibility = 'visible'
                weekDayCart.style.visibility = 'visible'
                orderTimeCart.style.visibility = 'visible'

                const html = '<div class="cart-item" name ="' + elt.item._id + '">\n' +
                    '<div class="cart-item-info">' +
                    '<img src="' + elt.item.picturePath + '" alt="product image">\n' +
                    '                         </div>' +
                    '                        <div class="cart-item-info">\n' +
                    '                            <button data-eventadded = false data-speciality ="' + elt.item._id + '" class="button button-cart minus-qty-item">' +
                    '                                  <i class="bx bx-minus"></i>' +
                    '                           </button>\n' +
                    '                            <input class="input-qty-cart" name ="' + elt.item._id + '" type="text" readonly size="1"\n' +
                    '                                   value="' + elt.qty + '"/>\n' +
                    '                            <button data-eventadded = false data-speciality ="' + elt.item._id + '" class="button button-cart plus-qty-item">' +
                    '                               <i class="bx bx-plus"></i>' +
                    '                           </button>\n' +
                    '                        </div>\n' +
                    '                        <div class="cart-item-info">\n' +
                    '                            <h3 class="cart-item-name">' + elt.item.name + '</h3>\n' +
                    '                            <span class="cart-item-price">' + elt.item.price + ' €</span>\n' +
                    '                        </div>\n' +
                    '\n' +
                    '                        <button data-eventadded = false data-speciality ="' + elt.item._id + '"type="button" class="cart-item-del-btn">\n' +
                    '                            <i class="fas fa-times"></i>\n' +
                    '                        </button>\n' +
                    '                    </div>'
                const cartTotal = document.querySelector('.cart-total');
                if (cartTotal !== null) {
                    orderDateContent.insertAdjacentHTML('beforebegin', html);
                } else {
                    cartList.innerHTML += html
                }
                let cartTotalValue = document.querySelector('#cart-total-value')
                if (cartTotalValue == null) {
                    const footerHtml =
                        '<div id="comment" class="form__group">\n' +
                        '    <textarea maxlength="50" class="orderComment form__field" placeholder="Commentaire pour la commande..." id="commentOrder"></textarea>\n' +
                        '<label for="commentOrder" class="form__label">Ajouter un commentaire pour votre commande</label>'+
                        '</div>' +
                        '<div class="cart-total">\n' +
                        '   <h3>Total : </h3>\n' +
                        '   <span id="cart-total-value"></span> €\n' +
                        '</div>\n' +
                        '<button id="cart-btn" class="button">Payer en ligne</button>' +
                        '<button id="cart-btn-cod" class="button">Payer sur place</button>'

                    addExtra.innerHTML += footerHtml
                    btnOrderBtn = document.querySelector('#cart-btn')
                    if (btnOrderBtn) {
                        btnOrderBtn.addEventListener('click', (e) => {
                            redirectCheckoutSessionPayment()
                        })
                    }
                    btnOrderCODBtn = document.querySelector('#cart-btn-cod')
                    if (btnOrderCODBtn) {
                        btnOrderCODBtn.addEventListener('click', (e) => {
                            order()
                        })
                    }
                }
                //minus action
                minusQtyItem = document.querySelectorAll('.minus-qty-item');
                minusQtyItem.forEach((btn) => {
                    let eventAdded = btn.dataset.eventadded
                    if (eventAdded === 'false') {
                        btn.addEventListener('click', (e) => {
                            let _idProduct = btn.dataset.speciality
                            updateCart(_idProduct, "minus")
                        })
                        btn.dataset.eventadded = true
                    }
                })
                //plus action
                plusQtyItem = document.querySelectorAll('.plus-qty-item');
                plusQtyItem.forEach((btn) => {
                    let eventAdded = btn.dataset.eventadded
                    if (eventAdded === 'false') {
                        btn.addEventListener('click', (e) => {
                            let _idProduct = btn.dataset.speciality
                            updateCart(_idProduct, "plus")
                        })
                        btn.dataset.eventadded = true
                    }

                })
                //minus action
                minusQtyItemExtra = document.querySelectorAll('.minus-qty-item-extra');
                minusQtyItemExtra.forEach((btn) => {
                    let eventAdded = btn.dataset.eventadded
                    if (eventAdded === 'false') {
                        btn.addEventListener('click', (e) => {
                            let _idProduct = btn.dataset.idspec
                            let speciality = btn.dataset.spec
                            let isExtra = btn.dataset.isextra
                            updateCart(_idProduct, "minus", isExtra, speciality)
                        })
                        btn.dataset.eventadded = true
                    }
                })
                //plus action
                plusQtyItemExtra = document.querySelectorAll('.plus-qty-item-extra');
                plusQtyItemExtra.forEach((btn) => {
                    let eventAdded = btn.dataset.eventadded
                    if (eventAdded === 'false') {
                        btn.addEventListener('click', (e) => {
                            let _idProduct = btn.dataset.idspec
                            let speciality = btn.dataset.spec
                            let isExtra = btn.dataset.isextra
                            updateCart(_idProduct, "plus", isExtra, speciality)
                        })
                        btn.dataset.eventadded = true
                    }

                })
                let deleteItemCart = document.querySelectorAll('.cart-item-del-btn');
                //delete item cart action
                deleteItemCart.forEach((btn) => {
                    let eventAdded = btn.dataset.eventadded
                    if (eventAdded === 'false') {
                        btn.addEventListener('click', (e) => {
                            let _idProduct = btn.dataset.speciality
                            updateCart(_idProduct, "delete")
                        })
                    }
                    btn.dataset.eventadded = true
                })
            } else {
                const identifiant = elt.item._id
                var el = document.querySelector("input[name='" + identifiant + "']");
                if (el) {
                    el.value = elt.qty
                }
            }
            let cartTotalValue = document.querySelector('#cart-total-value')
            if (cartTotalValue) {
                cartTotalValue.innerHTML = res.data.cartTotalPrice
            }

            let acc = document.querySelector(".accordion");
            if (acc.dataset.eventadded === 'false' && acc.dataset.initevent === 'false') {
                acc.dataset.eventadded = true
                acc.addEventListener("click", function () {
                    acc.classList.toggle("active");
                    var panel = acc.nextElementSibling;
                    if (panel.style.display === "block") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "block";
                    }
                });
            }

            new Noty({
                type: "success",
                layout: "bottomRight",
                timeout: 1000,
                progressBar: false,
                text: "1 " + elt.item.name + " a été ajouté à votre panier"
            }).show();
        } else {
            let res;
            if (isExtra) {
                res = await axios.post('/update-cart', {
                    "_id": spec,
                    "operator": operator,
                    "isExtra" :  isExtra,
                    "speciality" : JSON.parse(speciality)
                })
            } else {
                res = await axios.post('/update-cart', {"_id": spec, "operator": operator})
            }
            const identifiant = res.data._id
            let el = document.querySelector("input[name='" + identifiant + "']");
            if (el) {
                if (!isExtra) {
                    el.value = res.data.itemQty
                } else {
                    if (res.data.resetValue) {
                        let qtyCartInput = document.querySelectorAll('.input-qty-cart');
                        qtyCartInput.forEach((element) => {
                            element.value = 0
                        })
                    } else {
                        el.value = res.data.itemQtyExtra
                    }
                }

            }
            if (operator === 'delete') {
                let el = document.querySelector(".cart-item[name='" + identifiant + "']");
                if (el) {
                    el.remove()
                }
            }
            if (Object.keys(res.data.items).length === 0) {
                let divCartTotal = document.querySelector('.cart-total')
                let btnOrder = document.querySelector('#cart-btn')
                let btnCODOrder = document.querySelector('#cart-btn-cod')
                let comment = document.querySelector('#comment')
                if (divCartTotal) {
                    divCartTotal.remove()
                }
                if (btnOrder) {
                    btnOrder.remove()
                }
                if (btnCODOrder) {
                    btnCODOrder.remove()
                }
                if (comment) {
                    comment.remove()
                }
                let html = ' <div id="empty-cart">\n' +
                    '                        <div>\n' +
                    '                            <p style="text-align: center">Votre panier est vide </p>\n' +
                    '                        </div>\n' +
                    '                        <div class="emptyCart">\n' +
                    '                            <img style="width: 50%" src="img/emptyBol.png">\n' +
                    '                        </div>\n' +
                    '                    </div>'
                let emptyCart = document.querySelector('#empty-cart')
                if (!emptyCart) {
                    cartList.innerHTML += html
                }
                addExtra.style.display = 'none'
                weekDayCart.style.visibility = 'hidden'
                orderTimeCart.style.visibility = 'hidden'
                orderDateContent.style.visibility = 'hidden'
                let acc = document.querySelector(".accordion");
                acc.dataset.eventadded = false
                acc.dataset.initevent = false

                minusQtyItemExtra = document.querySelectorAll('.minus-qty-item-extra');
                minusQtyItemExtra.forEach((btn) => {
                    btn.dataset.eventadded = false
                })

                plusQtyItemExtra = document.querySelectorAll('.plus-qty-item-extra');
                plusQtyItemExtra.forEach((btn) => {
                    btn.dataset.eventadded = false
                })
            }
            let cartTotalValue = document.querySelector('#cart-total-value')
            if (cartTotalValue) {
                cartTotalValue.innerHTML = res.data.cartTotalPrice
            }
            cartCounter.innerHTML = res.data.totalQty
        }
    } catch (e) {
        console.log(e);
    }
}


async function signup() {
    try {
        let firstName = document.querySelector("input[name='firstName']").value;
        let lastName = document.querySelector("input[name='lastName']").value;
        let email = document.querySelector("input[name='email']").value;
        let password = document.querySelector("input[name='password']").value;
        let confirmPassword = document.querySelector("input[name='confirmPassword']").value;
        let captchaToken = document.querySelector("#captchaToken").value;

        if (!firstName || !lastName || !email || !password || !confirmPassword) {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Tous les champs sont obligatoires"
            }).show();
        } else {
            const user = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                confirmPassword: confirmPassword,
                captchaToken: captchaToken,
            }
            let res = await axios.post('/register', user)
            if (res.data.success === false) {
                new Noty({
                    timeout: 2000,
                    type: "error",
                    text: res.data.message
                }).show();
            } else {
                new Noty({
                    timeout: 4000,
                    type: "success",
                    text: res.data.message
                }).show();
            }

        }
    } catch (e) {
        console.log(e)
    }
}

async function signin() {
    try {
        let email = document.querySelector("input[name='loginEmail']").value;
        let password = document.querySelector("input[name='loginPassword']").value;

        const user = {
            email,
            password
        }
        let res = await axios.post('/login', user)
        if (res.data.success === false) {
            new Noty({
                timeout: 2000,
                type: "error",
                text: res.data.message
            }).show();
        } else {
            axios.post('/add-user-session', res.data.user).then(function () {
                new Noty({
                    timeout: 4000,
                    type: "success",
                    text: res.data.message
                }).show();

                sleep(2000).then(() => {
                    window.location = res.data.redirectURL
                });
            })
        }
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "La connexion est indisponible dû à un problème technique"
        }).show();
    }
}

async function logout() {
    try {
        let res = await axios.post('/logout')
        window.location = "/espace-membre";

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

async function editAccountData(idUser, token, fname, lname, email, phone) {
    try {
        const user = {
            idUser,
            token,
            fname,
            lname,
            email,
            phone
        }
        await axios.post('/edit-account', user)
        const res = await axios.post('/edit-user-session', user)

        new Noty({
            timeout: 2000,
            type: "success",
            text: res.data.message
        }).show();

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

async function changeOrder() {
    try {
        let userId = document.getElementById('userIdOrder').value
        let orderId = selectCurrentOrders.value
        const session = await axios.get('/check-user-session')

        const res = await axios.post('/order',
            {
                "token": session.data.user.token,
                "userId": userId,
                "orderId": orderId
            }
        )

        const order = res.data.order
        document.getElementById('lbOrderStatus').innerHTML = tabStatusOrdered[order.status]
        document.getElementById('dateOrder').innerHTML = Moment.tz(order.createdAt, 'Europe/Paris').format('D/M/Y HH:mm')
        document.getElementById('dateDelivered').innerHTML = Moment.tz(order.deliveredTime, 'Europe/Paris').format('D/M/Y HH:mm')
        document.getElementById('orderAmount').innerHTML = order.amount
        document.getElementById('orderStatus').innerHTML = tabStatusOrdered[order.status]
        document.getElementById('orderState').src = tabImgStatus[order.status]


    } catch (e) {
        console.log(e)
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

async function initSocket() {
    try {
        const session = await axios.get('/check-user-session')
        socket.emit('join', 'customer' + session.data.user._id)
    } catch (e) {
        console.log(e)
    }
}

async function redirectCheckoutSessionPayment() {
    await axios.post('/mustOpenSideCart')
    const  orderTimeValue = document.querySelector('#orderTimeCart').value
    let comment = document.querySelector('#commentOrder').value
    const result = await axios.post('/create-checkout-session', {
        "orderTimeValue": orderTimeValue,
        "comment": comment,
    })
    if (result.data.success === true) {
        window.location = result.data.urlRedirect;
    } else {
        if (result.data.urlRedirect) {
            window.location = result.data.urlRedirect;
        } else {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Un problème technique est rencontré. Le paiement est momentanément indisponible"
            }).show();
        }

    }

}


async function order() {
    try {
        const res = await axios.get('/check-user-session')
        if (!res.data.isExist) {
            await axios.post('/mustOpenSideCart')
            window.location = "/espace-membre";
        } else {
            const cart = await axios.get('/cart')
            let comment = document.querySelector('#commentOrder').value
            let orderTimeValue = document.querySelector('#orderTimeCart').value
            await axios.post('/orders', {
                "cart": cart.data.cart,
                "comment": comment,
                "orderTimeValue": orderTimeValue,
                "user": cart.data.user,
                "token": cart.data.user.token
            })

            new Noty({
                timeout: 2000,
                type: "success",
                text: "Votre commande a bien été enregistrée"
            }).show();

            await axios.post('/clear-cart')

            sleep(2000).then(() => {
                window.location = "/votre-commande"
            });
        }
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

function update_time_order() {
    let possibleOrderTime = JSON.parse(document.getElementById("possibleOrderTime").value)
    let weekdayValue = weekDayCart.value

    const  options = document.querySelectorAll('#orderTimeCart option');
    options.forEach(o => o.remove());

    const optgroup = document.querySelectorAll('#orderTimeCart optgroup');
    optgroup.forEach(o => o.remove());

    Object.keys(possibleOrderTime[weekdayValue]).forEach(function (k) {
        let opt = document.createElement('optgroup');
        opt.label = k
        orderTimeCart.appendChild(opt);
        Object.keys(possibleOrderTime[weekdayValue][k]).forEach(function (item) {
            let option = document.createElement('option');
            option.value = item;
            option.innerHTML = possibleOrderTime[weekdayValue][k][item];
            opt.appendChild(option);
        })
    })
}

//addToCart action
addToCart.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let speciality = btn.dataset.speciality
        updateCart(speciality)
    })
})

//minus qty action
minusQtyItem.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let _idProduct = btn.dataset.speciality
        updateCart(_idProduct, "minus")
    })
})

//plus qty action
plusQtyItem.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let _idProduct = btn.dataset.speciality
        updateCart(_idProduct, "plus")
    })
})

//minus qty action
minusQtyItemExtra.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let _idProduct = btn.dataset.idspec
        let speciality = btn.dataset.spec
        let isExtra = btn.dataset.isextra
        updateCart(_idProduct, "minus", isExtra, speciality)
    })
})

//plus qty action
plusQtyItemExtra.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let _idProduct = btn.dataset.idspec
        let speciality = btn.dataset.spec
        let isExtra = btn.dataset.isextra
        updateCart(_idProduct, "plus", isExtra, speciality)
    })
})

//delete item cart action
deleteItemCart.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        let _idProduct = btn.dataset.speciality
        updateCart(_idProduct, "delete")
    })
})

if (weekDayCart) {
    weekDayCart.addEventListener('change', () => {
        update_time_order()
    })
}
if (btnSignUp) {
    btnSignUp.addEventListener('click', (e) => {
        signup()
    })
}

if (btnSignIn) {
    btnSignIn.addEventListener('click', (e) => {
        signin()
    })
}

if (btnLogout) {
    btnLogout.addEventListener('click', (e) => {
        logout()
    })
}

if (btnOrderBtn) {
    btnOrderBtn.addEventListener('click', (e) => {
        redirectCheckoutSessionPayment()
    })
}

if (btnOrderCODBtn) {
    btnOrderCODBtn.addEventListener('click', (e) => {
        order()
    })
}


if (btnEditAccount) {
    btnEditAccount.addEventListener('click', (e) => {
        e.preventDefault();
        const _idUser = btnEditAccount.dataset.userid
        const token = btnEditAccount.dataset.usertoken
        const fname = document.getElementById('fname').value
        const lname = document.getElementById('lname').value
        const email = document.getElementById('emailContact').value
        const phone = document.getElementById('telephone').value

        editAccountData(_idUser, token, fname, lname, email, phone)
    })
}

if (selectCurrentOrders) {
    selectCurrentOrders.addEventListener('change', (e) => {
        changeOrder()
    })
}


// sleep time expects milliseconds
function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

initSocket()

socket.on('orderStatusChanged', (data) => {

    const currentOrders = document.getElementById('currentOrders')
    const totalCurrentOrder = document.getElementById('totalCurrentOrder')
    const statusOrder = data.orderStatus
    if (!currentOrders || currentOrders.value === data.orderId) {

        document.getElementById('lbOrderStatus').innerText = appConst.convertStatusOrder.status[statusOrder]
        document.getElementById('orderStatus').innerText = appConst.convertStatusOrder.status[statusOrder]
        document.getElementById('orderState').src = appConst.convertStatusOrder.image[statusOrder]

    }
    if (statusOrder === 'order_delivered') {
        if (parseInt(totalCurrentOrder.textContent) - 1 > 0) {
            totalCurrentOrder.innerHTML = parseInt(totalCurrentOrder.textContent) - 1
        } else {
            const orderAlert = document.getElementById('orderAlert')
            orderAlert.remove()
        }

        axios.post('/edit-currentOrder-session').then(function (data) {
            console.log(data)
        })
    }
    new Noty({
        timeout: 2000,
        type: "success",
        text: "Le statut de votre commande a été changé"
    }).show();
})

