import axios from "axios";
import Noty from "noty";

let fileCat = null
let btnCreateCategory = document.getElementById('createCategorySpeciality')
let catNameInput = document.getElementById('editCategoryName')
let catSubtitleInput = document.getElementById('editCategorySubtitle')
let catDescriptionInput = document.getElementById('editCategoryDescription')
let catPicturePathInput = document.getElementById('editCategoryPicturePath')
let idCatNameInput = document.getElementById('editIdCategoryName')
let editIsExtraInput = document.getElementById('editIsExtra')
let IsExtraInput = document.getElementById('isExtra')
let reader = {}

async function createSpecialityCategory() {
    try {
        let categoryName = document.querySelector("input[name='categoryName']").value;
        let subTitleCategory = document.querySelector("input[name='subTitleCategory']").value;
        let descriptionCategory = document.querySelector("textarea[name='descriptionCategory']").value;

        if (fileCat === null || categoryName==='' || subTitleCategory==='' || descriptionCategory==='') {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Tous les champs sont obligatoires"
            }).show();
        } else {
            const res = await axios.post('/admin/nouvelle-categorie', {
                "categoryName" : categoryName,
                "subTitleCategory" : subTitleCategory,
                "descriptionCategory" : descriptionCategory,
                "isExtra" : IsExtraInput.checked
            })

            if (res.data.success === true) {
                const idCategoryCreated = res.data.idCategory
                //add picture
                await start_upload(fileCat,idCategoryCreated, "category")
                new Noty({
                    timeout: 2000,
                    type: "success",
                    text: res.data.message
                }).show();
                //reset file
                fileCat = null
                sleep(2000).then(() => {
                    window.location.reload()
                });
            }
        }
    } catch (e) {
        console.log(e)
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

async function deleteCategorySpeciality(idCat) {
    try {
        if (confirm('Vous êtes sûr de vouloir supprimer cette catégorie ?'))  {
            const res = await axios.post('/admin/delete-category', {
                "idCategory": idCat,
            })

            document.getElementById("row-"+idCat).remove()

            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();
        }

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}

if (btnCreateCategory) {
    btnCreateCategory.addEventListener('click', (e) => {
        e.preventDefault();
        createSpecialityCategory()
    })
}

document.querySelectorAll('.editModalCatOpen').forEach(function (elt) {
    elt.addEventListener('click', function () {
        const idCat = elt.dataset.idcategory
        const nameCat = elt.dataset.namecategory
        const subTitleCat = elt.dataset.subtitlecategory
        const descriptionCat = elt.dataset.descriptioncategory
        const picturePathCat = elt.dataset.picturepathcategory
        const isExtra = elt.dataset.isextra
        catNameInput.value = nameCat
        catSubtitleInput.value = subTitleCat
        catDescriptionInput.innerText = descriptionCat
        catPicturePathInput.src = picturePathCat
        idCatNameInput.value = idCat
        isExtra === 'false' ? editIsExtraInput.checked = false : editIsExtraInput.checked = true

    })
})

document.querySelectorAll('.deleteSpecialityCat').forEach(function (elt) {
    elt.addEventListener('click', function () {
        const idCat = elt.dataset.idcategory
        deleteCategorySpeciality(idCat)
    })
})



document.querySelectorAll('.editCategory').forEach(function (elt) {
    elt.addEventListener('click', async function () {
        try {
            const idCat = idCatNameInput.value
            const nameCat = catNameInput.value
            const subtitleCat = catSubtitleInput.value
            const descriptionCat = catDescriptionInput.value
            const isExtra = editIsExtraInput.checked

            const res = await axios.post('/admin/update-category', {
                "idCat": idCat,
                "nameCat": nameCat,
                "subtitleCat": subtitleCat,
                "descriptionCat": descriptionCat,
                "isExtra" : isExtra
            })

            document.getElementById('catname_'+idCat).innerText = nameCat
            document.getElementById('catsubtitle_'+idCat).innerText = subtitleCat

            if (fileCat) {
                await start_upload(fileCat,idCat, "category")
                fileCat = null
            }

            sleep(2000).then(() => {
                window.location.reload()
            });
            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();

            //document.getElementById('catImage_'+idCat).src = nameCat
        } catch (e) {
            console.log(e)
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Une erreur s'est produite"
            }).show();
        }


    })
})

// sleep time expects milliseconds
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

FilePond.registerPlugin(
    FilePondPluginImagePreview,
);


const pondCat = FilePond.create(document.querySelector('.image-preview-filepond-cat'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});


const pondModalCat = FilePond.create(document.querySelector('.image-preview-filepond-modal-cat'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});

pondCat.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    fileCat = fileAdded.file
});

pondCat.on('removefile', () => {

    fileCat = null
});

pondModalCat.on('removefile', () => {

    fileCat = null
});

pondModalCat.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    fileCat = fileAdded.file
});

async function start_upload(file, idCategory, pictureOf) {
    reader = new FileReader();
    await upload_file(file, idCategory, pictureOf);
}

async function upload_file(file, idCategory, pictureOf) {
    reader.onloadend = async function (event) {
        await axios.post('/admin/uploads', {
            "fileData": event.target.result.split(',')[1],
            "fileName": file.name,
            "pictureOf": pictureOf,
            "idCategory": idCategory,
        })
    }
    reader.readAsDataURL(file)
}


