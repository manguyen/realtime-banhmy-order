import axios from "axios";
import Noty from "noty";
import moment from 'moment-timezone'
let btnLogout = document.querySelector('#logout')


async function logout() {
    try {
        await axios.post('/logout')
        window.location = "/espace-membre";

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}
async function updateNumbePlacedAndProcessingOrder (data) {
    try {
        const result = await axios.get('/getNumbePlacedAndProcessingOrder')
        const nbOrderPlaced = result.data.nbOrderPlaced
        const nbOrderProcessing = result.data.nbOrderProcessing
        document.getElementById('orderPlaced').innerText = nbOrderPlaced
        document.getElementById('orderProcessing').innerText = nbOrderProcessing
        if (data.orderStatus === 'order_delivered') {
            if (document.getElementById(`${'line_'+data.orderId}`).classList.contains('nopaidLine')){
                document.getElementById(`${'line_'+data.orderId}`).classList.remove('nopaidLine')
                document.getElementById(`${'line_'+data.orderId}`).classList.add('paidLine')
                document.getElementById('paymentStatus_'+data.orderId).innerHTML = 'Payé'
            }
        }
    } catch (e) {
        console.log(e)
    }

}
async function processChangeOrderStatus(orderStatus, orderId) {
    try {
        const session = await axios.get('/check-user-session')

        const res = await axios.post('/changeStatusOrder', {
            "orderStatus": orderStatus,
            "orderId": orderId,
            "token": session.data.user.token
        })

        new Noty({
            timeout: 2000,
            type: "success",
            text: res.data.message
        }).show();
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}

async function processScanQRCODEandChangeOrderStatus(orderId) {
    try {
        const session = await axios.get('/check-user-session')

        const resOrder = await axios.post('/order', {
            "orderId": orderId,
            "token": session.data.user.token
        })

        let order = resOrder.data.order

        let nextStatus = getNextOrderStatus(order.status)
        if (nextStatus !== false) {
            const resChangeStatus = await axios.post('/changeStatusOrder', {
                "orderStatus": nextStatus,
                "orderId": orderId,
                "token": session.data.user.token
            })
            document.querySelector("select[name='" + orderId + "']").value = nextStatus;
            new Noty({
                timeout: 2000,
                type: "success",
                text: resChangeStatus.data.message
            }).show();
        } else {
            new Noty({
                timeout: 2000,
                type: "warning",
                text: "Cette commande a été livrée"
            }).show();
        }

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}

function getNextOrderStatus(status) {
    let nextStatus

    switch (status) {
        case "order_placed" :
            nextStatus = "order_processing"
            break;
        case "order_processing":
            nextStatus = "order_done"
            break
        case "order_done" :
            nextStatus = "order_delivered"
            break
        default:
            nextStatus = false
            break;
    }

    return nextStatus
}

async function changeStatus(orderStatus, orderId, paymentStatus) {
    try {
        if (orderStatus === 'order_delivered') {
            if (paymentStatus === 'NOPAID') {
                if (confirm('Cette commande a été payée ?'))  {
                    await processChangeOrderStatus(orderStatus, orderId);
                }
            } else {
                await processChangeOrderStatus(orderStatus, orderId);
            }
        } else {
            await processChangeOrderStatus(orderStatus, orderId);
        }
    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }

}

if (btnLogout) {
    btnLogout.addEventListener('click', (e) => {
        logout()
    })
}

document.querySelectorAll('.statusChange').forEach(function (elt) {
    elt.addEventListener('change', function () {
        const paymentStatus = elt.dataset.paymentstatus
        changeStatus(elt.value, elt.id, paymentStatus)
    })
})

function addRow(tableID, data) {
    // Get a reference to the table
    let tableRef = document.getElementById(tableID);

    // Insert a row at the end of the table
    let newRow = tableRef.insertRow(-1);

    newRow.id = 'line_'+data.order._id
    newRow.title = data.order.comment !== '' ? data.order.comment : 'Aucun commentaire'

    newRow.classList.add('lineOrderTable')

    if (data.order.paymentStatus === 'PAID') {
        newRow.classList.add('paidLine')
    } else{
        newRow.classList.add('nopaidLine')
    }

    // Insert a cell in the row at index 0
    let  numberOrderColumn =  newRow.insertCell(0);
    let  orderColumn =  newRow.insertCell(1);
    let  clientColumn = newRow.insertCell(2);
    let  paymentStatusColumn = newRow.insertCell(3);
    let  amountColumn = newRow.insertCell(4);
    let  createdAtColumn =    newRow.insertCell(5);
    let  deliveredAtColumn =  newRow.insertCell(6);
    let  orderStatusColumn =  newRow.insertCell(7);



    // Append a text node to the cell
    let orderNumber = document.createTextNode(data.order.orderNumber);
    let  contentOrder = ''
    Object.keys(data.order.items).forEach(function (k) {
        let item = '<p>'+ data.order.items[k].qty + ' ' + data.order.items[k].item.name+'</p>'
        contentOrder+=item
    })
    if (data.order.extras) {
        Object.keys(data.order.extras).forEach(function (k) {
            let extra = '<p>'+ data.order.extras[k].qty + ' ' + data.order.extras[k].item.speciality.name+'</p>'
            contentOrder+=extra
        })
    }
    let client = document.createTextNode(data.name);
    let paymentStatus = `<span id="paymentStatus_${data.order._id}">${data.order.paymentStatus ==='PAID' ? 'Payé' : 'Non Payé'}</span>`
    let amount = document.createTextNode(data.order.amount + ' €');
    let orderCreatedAt = document.createTextNode(moment.tz(data.order.createdAt, "Europe/Paris").format('D/M/Y HH:mm'));
    let orderDeliveredAt = document.createTextNode(moment.tz(data.order.deliveredTime, "Europe/Paris").format('D/M/Y HH:mm'));
    let orderCurrentStatus = ` <fieldset class="form-group">
                                    <select class="form-select statusChange" name= "${data.order._id}"id="${data.order._id}" data-paymentstatus="${data.order.paymentStatus}">
                                         <option value="order_placed" selected>En attente</option>
                                         <option value="order_processing" >En cours</option>
                                         <option value="order_done">Terminé</option>
                                         <option value="order_delivered">Livré</option>
                                    </select>
                               </fieldset>`;
    numberOrderColumn.appendChild(orderNumber)
    orderColumn.innerHTML = contentOrder;
    clientColumn.appendChild(client);
    paymentStatusColumn.innerHTML = paymentStatus;
    amountColumn.appendChild(amount);
    createdAtColumn.appendChild(orderCreatedAt);
    deliveredAtColumn.appendChild(orderDeliveredAt);
    orderStatusColumn.innerHTML = orderCurrentStatus;

    document.getElementById(`${data.order._id}`).addEventListener('change', function () {
        const paymentStatus = this.dataset.paymentstatus
        changeStatus(this.value, this.id, paymentStatus)
    })
}


//socket
let socket = io()
// Join
socket.emit('join', 'administratorRoom')

socket.on('orderNewPlaced', (data) => {

    const orderPlaced = document.getElementById('orderPlaced')
    const incomeOfTheDay = document.getElementById('incomeOfTheDay')
    const numberOrderOfTheDay = document.getElementById('numberOrderOfTheDay')

    ++orderPlaced.textContent
    ++numberOrderOfTheDay.textContent
    let incomeAmount = parseInt(incomeOfTheDay.textContent)  + data.order.amount
    incomeOfTheDay.innerHTML = incomeAmount

     addRow('currentDashboardOrderTable', data)


    new Noty({
        timeout: 2000,
        type: "success",
        text: "Une nouvelle commande de " + data.order.amount + " € vient d'être passée par " + data.name
    }).show();
})


socket.on('orderStatusChanged', (data) => {
    updateNumbePlacedAndProcessingOrder(data)
})

let scanner = new Instascan.Scanner({video : document.getElementById('preview')})
Instascan.Camera.getCameras().then(function (cameras) {
    if(cameras.length > 0) {
        scanner.start(cameras[0])
    } else {
        alert('No camera found')
    }
}).catch(function (e) {
    console.log(e)
})

scanner.addListener('scan', async function (orderId) {
    await processScanQRCODEandChangeOrderStatus(orderId.substring(1, orderId.length-1))
})
