import Noty from "noty";
import axios from "axios";
let opTable = document.querySelector('#openingTimeTable');
if (opTable) {
    var dataOpTable = new simpleDatatables.DataTable(opTable);
}
let secondServiceCb = document.getElementById('secondService')
let btnConfigure = document.getElementById('config')
let updateOpt = document.querySelector('.editOpeningTime')

if (secondServiceCb) {
    secondServiceCb.addEventListener('click', function () {
        document.querySelectorAll('.hiddenBlock').forEach(function (elt) {
            if (secondServiceCb.checked) {
                elt.style.display = 'block'
            } else {
                elt.style.display = 'none'
            }
        })
    })
}

if (btnConfigure) {
    btnConfigure.addEventListener('click', (e) => {
        e.preventDefault();
        configureOpenDay()
    })
}

async function configureOpenDay() {
    try {
        let service = {}, services = {};
        let error = false
        let startTimeFS = document.querySelector("input[name='startTimeFirstService']").value;
        let endTimeFS = document.querySelector("input[name='endTimeFirstService']").value;
        let isCloseChecked = document.querySelector("#isClose").checked;
        let weekDay = document.querySelector("#weekDay").value;

        if (startTimeFS === '' || endTimeFS === '') {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Veuillez remplir les horaires du premier service"
            }).show();
        } else {
             service = {
                 "startTime": startTimeFS,
                 "endTime": endTimeFS
             }

            services["Service 1"] = service
            let cbSecondServiceChecked = document.querySelector("#secondService").checked;

            if (cbSecondServiceChecked) {
                let startTimeSS = document.querySelector("input[name='startTimeSecondService']").value;
                let endTimeSS = document.querySelector("input[name='endTimeSecondService']").value;
                if (startTimeSS === '' || endTimeSS === '') {
                    error = true
                } else {
                    service = {
                        "startTime": startTimeSS,
                        "endTime": endTimeSS
                    }
                    services["Service 2"] = service
                }
            }
            if (!error) {
                const res = await axios.post('/admin/add-new-openday', {
                    "weekDay" : weekDay,
                    "services" : services,
                    "isClose" : isCloseChecked,
                })

                if (res.data.success === true) {
                    new Noty({
                        timeout: 2000,
                        type: "success",
                        text: res.data.message
                    }).show();

                    sleep(2000).then(() => {
                        window.location.reload()
                    });
                }
            } else {
                new Noty({
                    timeout: 2000,
                    type: "error",
                    text: "Veuillez remplir les horaires du deuxième service"
                }).show();
            }
        }
    } catch (e) {
        console.log(e)
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

if (dataOpTable) {
    dataOpTable.on('datatable.init', function () {
        document.querySelectorAll('.editModalOpeningTime').forEach(function (elt) {
            elt.addEventListener('click', function () {
                const openingTime = JSON.parse(elt.dataset.opt)
                document.getElementById('weekDayModal').value = openingTime.weekday
                document.getElementById('isCloseModal').checked = openingTime.isClose
                document.getElementById('startTimeFsModal').value = openingTime['services']['Service 1']['startTime']
                document.getElementById('endTimeFsModal').value = openingTime['services']['Service 1']['endTime']
                document.getElementById('startTimeSsModal').value = openingTime['services']['Service 2'] !== undefined  ? openingTime['services']['Service 2']['startTime'] : ''
                document.getElementById('endTimeSsModal').value = openingTime['services']['Service 2'] !== undefined ? openingTime['services']['Service 2'] ['endTime']: ''
            })
        })
    })
}

if (updateOpt) {
    updateOpt.addEventListener('click', async function () {
        try {
            const weekday =  document.getElementById('weekDayModal').value
            const isClose =  document.getElementById('isCloseModal').checked
            const startTimeFs =   document.getElementById('startTimeFsModal').value
            const endTimeFs =     document.getElementById('endTimeFsModal').value
            const startTimeSs =  document.getElementById('startTimeSsModal').value
            const endTimeSs =  document.getElementById('endTimeSsModal').value


            const res = await axios.post('/admin/update-openingtime', {
                "weekday": weekday,
                "isClose": isClose,
                "startTimeFs": startTimeFs,
                "endTimeFs": endTimeFs,
                "startTimeSs": startTimeSs,
                "endTimeSs": endTimeSs,
            })

            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();

            sleep(2000).then(() => {
                window.location.reload()
            });

        } catch (e) {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Une erreur s'est produite"
            }).show();
        }


    })
}
// sleep time expects milliseconds
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}
