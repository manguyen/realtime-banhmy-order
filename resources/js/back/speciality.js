import axios from "axios";
import Noty from "noty";
let specTable = document.querySelector('#specTable');
if (specTable) {
    var dataTable = new simpleDatatables.DataTable(specTable);
}


let fileSpec = null
let btnCreateSpeciality = document.getElementById('createSpeciality')
let editSpecNameInput = document.getElementById('editSpecName')
let editSpecDescriptionInput = document.getElementById('editSpecDescription')
let editSpecPriceInput = document.getElementById('editSpecPrice')
let editSpecTVAInput = document.getElementById('editSpecTVA')
let editIsSpecialityAvailableInput = document.getElementById('editIsSpecialityAvailable')
let editIdSpecNameInput = document.getElementById('editIdSpecName')
let editSpecPicturePath = document.getElementById('editSpecPicturePath')
let reader = {}

async function createSpeciality() {
    try {
        let specName = document.querySelector("input[name='speciliatyName']").value;
        let specPrice = document.querySelector("input[name='speciliatyPrice']").value;
        let specDescription = document.querySelector("textarea[name='specialityDescription']").value;
        let specTVA = document.querySelector("input[name='specialityTVA']").value;
        let selectCategory = document.getElementById("categorySelect");
        let idCategory = selectCategory.options[selectCategory.selectedIndex].value;

        if (fileSpec === null || specName==='' || specPrice==='' || specDescription===''||specTVA ==='') {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Tous les champs sont obligatoires"
            }).show();
        } else {
            const res = await axios.post('/admin/nouveau-plat', {
                "specName" : specName,
                "specPrice" : specPrice,
                "specDescription" : specDescription,
                "specTVA" : specTVA,
                "idCategory" : idCategory,
            })

            if (res.data.success === true) {
                const idSpeciality = res.data.idSpeciality
                //add picture
                await start_upload(fileSpec,idSpeciality, "speciality")
                new Noty({
                    timeout: 2000,
                    type: "success",
                    text: res.data.message
                }).show();
                //reset file
                fileSpec = null
                sleep(2000).then(() => {
                    window.location.reload()
                });
            }
        }
    } catch (e) {
        console.log(e)
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Un problème technique est survenu"
        }).show();
    }
}

async function deleteSpeciality(idSpec) {
    try {
        if (confirm('Vous êtes sûr de vouloir supprimer ce plat ?'))  {
            const res = await axios.post('/admin/delete-speciality', {
                "idSpec": idSpec,
            })

            document.getElementById("row-"+idSpec).remove()

            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();
        }

    } catch (e) {
        new Noty({
            timeout: 2000,
            type: "error",
            text: "Une erreur s'est produite"
        }).show();
    }
}

if (btnCreateSpeciality) {
    btnCreateSpeciality.addEventListener('click', (e) => {
        e.preventDefault();
        createSpeciality()
    })
}

if (dataTable) {
    dataTable.on('datatable.init', function () {
        document.querySelectorAll('.editModalSpecOpen').forEach(function (elt) {
            elt.addEventListener('click', function () {
                const idSpec = elt.dataset.idspeciality
                const specName = elt.dataset.specname
                const specDescription = elt.dataset.specdescription
                const specPrice = elt.dataset.specprice
                const specTVA = elt.dataset.spectva
                const specPicturePath = elt.dataset.specpicturepath
                const isAvailable = elt.dataset.specisavailable

                editSpecNameInput.value = specName
                editSpecPriceInput.value = specPrice
                editSpecTVAInput.value = specTVA
                editSpecDescriptionInput.innerText = specDescription
                editSpecPicturePath.src = specPicturePath
                editIdSpecNameInput.value = idSpec
                isAvailable === 'false' ? editIsSpecialityAvailableInput.checked = false : editIsSpecialityAvailableInput.checked = true
            })
        })
    })

}

if (dataTable) {
    dataTable.on('datatable.page', function () {
        document.querySelectorAll('.editModalSpecOpen').forEach(function (elt) {
            elt.addEventListener('click', function () {
                const idSpec = elt.dataset.idspeciality
                const specName = elt.dataset.specname
                const specDescription = elt.dataset.specdescription
                const specPrice = elt.dataset.specprice
                const specTVA = elt.dataset.spectva
                const specPicturePath = elt.dataset.specpicturepath
                const isAvailable = elt.dataset.specisavailable

                editSpecNameInput.value = specName
                editSpecPriceInput.value = specPrice
                editSpecTVAInput.value = specTVA
                editSpecDescriptionInput.innerText = specDescription
                editSpecPicturePath.src = specPicturePath
                editIdSpecNameInput.value = idSpec
                isAvailable === 'false' ? editIsSpecialityAvailableInput.checked = false : editIsSpecialityAvailableInput.checked = true
            })
        })
    })
}

if (dataTable) {
    dataTable.on('datatable.sort', function () {
        document.querySelectorAll('.editModalSpecOpen').forEach(function (elt) {
            elt.addEventListener('click', function () {
                const idSpec = elt.dataset.idspeciality
                const specName = elt.dataset.specname
                const specDescription = elt.dataset.specdescription
                const specPrice = elt.dataset.specprice
                const specTVA = elt.dataset.spectva
                const specPicturePath = elt.dataset.specpicturepath
                const isAvailable = elt.dataset.specisavailable

                editSpecNameInput.value = specName
                editSpecPriceInput.value = specPrice
                editSpecTVAInput.value = specTVA
                editSpecDescriptionInput.innerText = specDescription
                editSpecPicturePath.src = specPicturePath
                editIdSpecNameInput.value = idSpec
                isAvailable === 'false' ? editIsSpecialityAvailableInput.checked = false : editIsSpecialityAvailableInput.checked = true
            })
        })
    })

}

if (dataTable) {
    dataTable.on('datatable.search', function () {
        document.querySelectorAll('.editModalSpecOpen').forEach(function (elt) {
            elt.addEventListener('click', function () {
                const idSpec = elt.dataset.idspeciality
                const specName = elt.dataset.specname
                const specDescription = elt.dataset.specdescription
                const specPrice = elt.dataset.specprice
                const specTVA = elt.dataset.spectva
                const specPicturePath = elt.dataset.specpicturepath
                const isAvailable = elt.dataset.specisavailable

                editSpecNameInput.value = specName
                editSpecPriceInput.value = specPrice
                editSpecTVAInput.value = specTVA
                editSpecDescriptionInput.innerText = specDescription
                editSpecPicturePath.src = specPicturePath
                editIdSpecNameInput.value = idSpec
                isAvailable === 'false' ? editIsSpecialityAvailableInput.checked = false : editIsSpecialityAvailableInput.checked = true
            })
        })
    })

}

document.querySelectorAll('.deleteSpeciality').forEach(function (elt) {
    elt.addEventListener('click', function () {
        const idCat = elt.dataset.idspeciality
        deleteSpeciality(idCat)
    })
})



document.querySelectorAll('.editSpec').forEach(function (elt) {
    elt.addEventListener('click', async function () {
        try {
            const specName = editSpecNameInput.value
            const specPrice = editSpecPriceInput.value
            const specTVA = editSpecTVAInput.value
            const specDescription = editSpecDescriptionInput.value
            const idSpec = editIdSpecNameInput.value
            const isAvailable = editIsSpecialityAvailableInput.checked

            const res = await axios.post('/admin/update-speciality', {
                "idSpec": idSpec,
                "specName": specName,
                "specPrice": specPrice,
                "specTVA": specTVA,
                "specDescription": specDescription,
                "isAvailable": isAvailable,
            })

            if (fileSpec) {
                await start_upload(fileSpec,idSpec, "speciality")
                fileSpec = null
            }

            sleep(2000).then(() => {
                window.location.reload()
            });
            new Noty({
                timeout: 2000,
                type: "success",
                text: res.data.message
            }).show();

            //document.getElementById('catImage_'+idCat).src = nameCat
        } catch (e) {
            new Noty({
                timeout: 2000,
                type: "error",
                text: "Une erreur s'est produite"
            }).show();
        }


    })
})

// sleep time expects milliseconds
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

FilePond.registerPlugin(
    FilePondPluginImagePreview,
);


const pondSpec = FilePond.create(document.querySelector('.image-preview-filepond-spec'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});


const pondModalSpec = FilePond.create(document.querySelector('.image-preview-filepond-modal-spec'), {
    allowImagePreview: true,
    allowImageFilter: false,
    allowImageExifOrientation: false,
    allowImageCrop: false,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise
        resolve(type);
    })
});

pondSpec.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    fileSpec = fileAdded.file
    console.log(file)
});

pondSpec.on('removefile', () => {

    fileSpec = null
});

pondModalSpec.on('removefile', () => {

    fileSpec = null
});

pondModalSpec.on('addfile', (error, fileAdded) => {
    if (error) {
        return;
    }
    fileSpec = fileAdded.file
});

async function start_upload(file, idSpeciality, pictureOf) {
    reader = new FileReader();
    await upload_file(file, idSpeciality, pictureOf);
}

async function upload_file(file, idSpeciality, pictureOf) {
    reader.onloadend = async function (event) {
        await axios.post('/admin/uploads', {
            "fileData": event.target.result.split(',')[1],
            "fileName": file.name,
            "pictureOf": pictureOf,
            "idSpeciality": idSpeciality,
        })
    }
    reader.readAsDataURL(file)
}


